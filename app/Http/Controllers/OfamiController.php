<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;

class OfamiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($locale)
    {
        $active = "index";
        if ($locale == "zh-TW") {
            $title = "樂享學 O-Fami｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車";
        } else if ($locale == "en") {
            $title = "O-Fami｜Cafe' & Baby Carriage & E-Bike";
        } else {
            $title = "O-Fami｜Cafe' & Baby Carriage & E-Bike";
        }
        App::setLocale($locale);
        return view('ofami.index', compact('active', 'title', 'locale'));
    }
    public function nikimotion($locale)
    {
        $active = "nikimotion";
        if ($locale == "zh-TW") {
            $title = "Nikimotion - Lite & Autofold & Blade & 配件｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Nikimotion - Lite & Autofold & Blade & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Nikimotion - Lite & Autofold & Blade & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.nikimotion', compact('active', 'title', 'locale'));
    }
    public function nkacc($locale)
    {
        $active = "nkacc";
        if ($locale == "zh-TW") {
            $title = "Nikimotion 配件 - Lite & Autofold & Blade｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Nikimotion Accessories - Lite & Autofold & Blade｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Nikimotion Accessories - Lite & Autofold & Blade｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.nkacc', compact('active', 'title', 'locale'));
    }
    public function nkaf($locale)
    {
        $active = "nkaf";
        if ($locale == "zh-TW") {
            $title = "Nikimotion - Autofold - 產品與配件｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Nikimotion - Autofold & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Nikimotion - Autofold & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.nkaf', compact('active', 'title', 'locale'));
    }
    public function nkafl($locale)
    {
        $active = "nkafl";
        if ($locale == "zh-TW") {
            $title = "Nikimotion - Lite - 產品與配件｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Nikimotion - Lite & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Nikimotion - Lite & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.nkafl', compact('active', 'title', 'locale'));
    }
    public function blade($locale)
    {
        $active = "blade";
        if ($locale == "zh-TW") {
            $title = "Nikimotion - Blade - 產品與配件｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Nikimotion - Blade & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Nikimotion - Blade & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.blade', compact('active', 'title', 'locale'));
    }
    public function veer($locale)
    {
        $active = "veer";
        if ($locale == "zh-TW") {
            $title = "Veer - Cruiser｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Veer - Cruiser & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Veer - Cruiser & Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);

        return view('ofami.veer', compact('active', 'title', 'locale'));
    }
    public function veeracc($locale, $type)
    {
        $active = "veeracc";
        if ($locale == "zh-TW") {
            if ($type == "1") {
                $title = "Veer - 置物籃｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "2") {
                $title = "Veer - 遮陽蓬｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "3") {
                $title = "Veer - 汽座轉接座｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "4") {
                $title = "Veer - 旅行袋｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "5") {
                $title = "Veer - 水杯架｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "6") {
                $title = "Veer - 軟座墊｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "7") {
                $title = "Veer - 餐盤｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else {
                $title = "Veer - 產品與配件｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            }
        } else if ($locale == "en") {
            if ($type == "1") {
                $title = "Veer - Foldable Storage Basket｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "2") {
                $title = "Veer - Retractable Canopy｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "3") {
                $title = "Veer - Infant Car Seat Adapter｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "4") {
                $title = "Veer - Travel Bag｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "5") {
                $title = "Veer - Cup Holders(set of 2)｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "6") {
                $title = "Veer - Comfort Seat｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "7") {
                $title = "Veer - Drink & Snack｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else {
                $title = "Veer Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            }
        } else {
            $title = "Veer Accessories｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.veeracc', compact('active', 'title', 'type', 'locale'));
    }
    public function ebike($locale)
    {
        $active = "ebike";
        if ($locale == "zh-TW") {
            $title = "電動輔助自行車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.ebike', compact('active', 'title', 'locale'));
    }
    public function ebikeinfo($locale, $type)
    {
        $active = "ebikeinfo";
        if ($locale == "zh-TW") {
            if ($type == "ebfat") {
                $title = "ikin-胖胎登山越野車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "ebflat") {
                $title = "ikin-平把變速公路車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "ebcurve") {
                $title = "ikin-彎把競速公路車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "ebknk") {
                $title = "K&K-胖胎登山越野車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "eb26") {
                $title = "ikin ez i-bike｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($type == "eb20") {
                $title = "巧創-20吋小徑城市悠遊摺疊車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else {
                $title = "電動輔助自行車｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            }
        } else if ($locale == "en") {
            if ($type == "ebfat") {
                $title = "Ikin - Big Max E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "ebflat") {
                $title = "Ikin - Meton E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "ebcurve") {
                $title = "Ikin - Hyper E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "ebknk") {
                $title = "K&K ｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "eb26") {
                $title = "Ikin- 26&quot; E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($type == "eb20") {
                $title = "Ballistic - 20&quot; E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else {
                $title = "E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            }
        } else {
            $title = "E-Bike｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.ebinfo', compact('active', 'title', 'type', 'locale'));
    }
    public function cafe($locale)
    {
        $active = "cafe";
        if ($locale == "zh-TW") {
            $title = "菜單｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Menu｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Menu｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.cafe', compact('active', 'title', 'locale'));
    }
    public function bikerental($locale, $nav)
    {
        $active = "bikerental";
        if ($locale == "zh-TW") {
            if ($nav == "index") {
                $title = "租車首頁｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($nav == "rundown") {
                $title = "租車流程｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($nav == "guide") {
                $title = "專業導覽｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else if ($nav == "discount") {
                $title = "優惠活動｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            } else {
                $title = "租車旅遊｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
            }
        } else if ($locale == "en") {
            if ($nav == "index") {
                $title = "Rental Index｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($nav == "rundown") {
                $title = "Rental Rundown｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($nav == "guide") {
                $title = "Rental Guide｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else if ($nav == "discount") {
                $title = "Rental Discount｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            } else {
                $title = "Bike Rental｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
            }
        } else {
            $title = "Menu｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        $section = $nav;
        App::setLocale($locale);
        return view('ofami.bikerental', compact('active', 'title', 'locale', 'nav', 'section'));
    }
    public function service($locale)
    {
        $active = "service";
        if ($locale == "zh-TW") {
            $title = "關於我們｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "About Us｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "About Us｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.service', compact('active', 'title', 'locale'));
    }
    public function tsip($locale)
    {
        $active = "tsip";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 南科幾米公園線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - Tainan Science-based Industrial Park｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - Tainan Science-based Industrial Park｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.tsip', compact('active', 'title', 'locale'));
    }
    public function ysr($locale)
    {
        $active = "ysr";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 鹽水溪山海圳線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - Shanhaizhen Greenway｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - Shanhaizhen Greenway｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.ysr', compact('active', 'title', 'locale'));
    }
    public function sdt($locale)
    {
        $active = "sdt";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 台江公園線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - Taijiang National Park｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - Taijiang National Park｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.sdt', compact('active', 'title', 'locale'));
    }
    public function hmtsip($locale)
    {
        $active = "hmtsip";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 歷史博物館線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - National Museum of Taiwan History｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - National Museum of Taiwan History｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.hmtsip', compact('active', 'title', 'locale'));
    }
    public function xh($locale)
    {
        $active = "xh";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 新化老街線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - Xinhua Old Street｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - Xinhua Old Street｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.xh', compact('active', 'title', 'locale'));
    }
    public function apcm($locale)
    {
        $active = "apcm";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 經安平至奇美博物館線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - Anping and Chimei Museum｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - Anping and Chimei Museum｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.apcm', compact('active', 'title', 'locale'));
    }
    public function wst($locale)
    {
        $active = "wst";
        if ($locale == "zh-TW") {
            $title = "旅遊路線圖 - 烏山頭水庫線｜咖啡輕食餐廳 & 嬰兒車 & E-Bike電動輔助自行車｜O-Fami 樂享學";
        } else if ($locale == "en") {
            $title = "Travel Route Map - Wushantou Reservoir｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        } else {
            $title = "Travel Route Map - Wushantou Reservoir｜Cafe' & Baby Carriage & E-Bike｜O-Fami";
        }
        App::setLocale($locale);
        return view('ofami.trip.wst', compact('active', 'title', 'locale'));
    }

    //測試用
    public function test()
    {
        $active = "test";
        $title  = "測試功能頁面|樂享學 O-Fami";
        $locale = "zh-TW";
        return view('ofami.test', compact('active', 'title', 'locale'));
    }
    public function taiwan()
    {
        $active = "taiwan";
        $title  = "台灣地圖測試|樂享學 O-Fami";
        $locale = "zh-TW";
        return view('ofami.taiwan', compact('active', 'title', 'locale'));
    }
    public function rentplace($rentplace)
    {
        $active = "rentplace";
        $title  = "租車點Rentplace|樂享學 O-Fami";
        $locale = "zh-TW";

        return view('ofami.rentplace', compact('active', 'title', 'rentplace', 'locale'));
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
