<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::get('/home/{locale}', 'OfamiController@index')->name('index');
Route::get('/', function () {
    return redirect('/home/zh-TW');
})->name('ofami.index');
//nk
Route::get('/nikimotion/{locale}', 'OfamiController@nikimotion')->name('ofami.nikimotion');
Route::get('/nk_acc/{locale}', 'OfamiController@nkacc')->name('ofami.nkacc');
Route::get('/nk_af/{locale}', 'OfamiController@nkaf')->name('ofami.nkaf');
Route::get('/nk_afl/{locale}', 'OfamiController@nkafl')->name('ofami.nkafl');
Route::get('/nk_blade/{locale}', 'OfamiController@blade')->name('ofami.blade');
//veer
Route::get('/veer/{locale}', 'OfamiController@veer')->name('ofami.veer');
Route::get('/veeracc/{locale}/{type}', 'OfamiController@veeracc')->name('ofami.veeracc');

//ebike
Route::get('/ebike/{locale}', 'OfamiController@ebike')->name('ofami.ebike');
Route::get('/ebike/{locale}/{type}', 'OfamiController@ebikeinfo')->name('ofami.ebikeinfo');
//cafe
Route::get('/cafe/{locale}', 'OfamiController@cafe')->name('ofami.cafe');
//bikerent
Route::get('/bikerental/{locale}/{nav}', 'OfamiController@bikerental')->name('ofami.bikerental');

Route::get('/trip_tsip/{locale}', 'OfamiController@tsip')->name('ofami.tsip');
Route::get('/trip_ysr/{locale}', 'OfamiController@ysr')->name('ofami.ysr');
Route::get('/trip_sdt/{locale}', 'OfamiController@sdt')->name('ofami.sdt');
Route::get('/trip_hmtsip/{locale}', 'OfamiController@hmtsip')->name('ofami.hmtsip');
Route::get('/trip_xh/{locale}', 'OfamiController@xh')->name('ofami.xh');
Route::get('/trip_apcm/{locale}', 'OfamiController@apcm')->name('ofami.apcm');
Route::get('/trip_wst/{locale}', 'OfamiController@wst')->name('ofami.wst');

Route::get('/service/{locale}', 'OfamiController@service')->name('ofami.service');

Route::get('/taiwan', 'OfamiController@taiwan')->name('ofami.taiwan');
Route::get('/rentplace/{rentplace}', 'OfamiController@rentplace')->name('ofami.rentplce');

Route::get('/test', 'OfamiController@test')->name('ofami.test');

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
