@extends("ofami.main")
@section('content')
@include("ofami.css.imghover")
<link rel="stylesheet" href="{{asset('css/flexslider/flexslider.css')}}" type="text/css">
<style>
	.container h1{
		font-size:35px;
	}
	.row h2{
		font-size:28px;
	}
	.price{
		font-family:price;
		font-size: 30px;
		float: right;
	}
	.price1{
		font-family:price;
		font-size: 20px;
	}
	.row ol{
		list-style-image:url('../images/veerlogo1.png');
		line-height: 38px;
	}
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--Veer-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Veer </h1>
		<iframe class="wow jackInTheBox" data-wow-duration="2s" width="100%" height="500" src="https://www.youtube.com/embed/JrIVwVhh7Nw?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</section>
<!--//Veer-->

<!--Veer_product-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Cruiser </h1>
		<div class="row">
			<div class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8 flexslider">
					<!--
					<a class="image-zoom" href="{{asset('images/veergif1.gif')}}" rel="prettyPhoto[gallery]">
						<div class="imgback">
							<img src="{{asset('images/veergif1.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
							<div class="overlay">
								<div class="text">
									<span class="fas fa-search"></span>
								</div>
							</div>
						</div>
					</a>-->
						<ul class="slides wow jackInTheBox" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veercruiser1.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veercruiser1b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veercruiser1a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veercruiser2.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veercruiser2b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veercruiser2a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veercruiser3.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veercruiser3b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veercruiser3a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veercruiser4.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veercruiser4b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veercruiser4a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veercruiser5.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veercruiser5b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veercruiser5a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					<p><br></p>
				</div>
				<div class="col-lg-2"></div>
				<!--
				<p class="price wow fadeInUp">NTD. 19,999　</p><br>
				<a href="http://www.pcstore.com.tw/ofami/M45904417.htm" class="btn btn-outline-primary" target="_blank" role="button" style="float:right;"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<p><br></p>-->
			</div>
			<div class="col-sm-6 wow fadeInUp" data-wow-duration="2s" align="left">
				{!!trans('veer.p1')!!}
				<br>
				<a href="http://www.pcstore.com.tw/ofami/M45904417.htm" class="btn btn-outline-primary" target="_blank" role="button" style="float:left;"><i class="fa fa-shopping-cart"></i> 立即購買</a>
			</div>
		</div>
	</div>
</section>
<!--Veer_product-->

<!--Accessories-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 id="acc" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Accessories 周邊配件 </h1>
		<div class="row">
			<div id="v1" class="col-6 col-lg-3 wow fadeInRight" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="1"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc1')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v2" class="col-6 col-lg-3 wow fadeInUp" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="2"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer2.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc2')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v3" class="col-6 col-lg-3 wow fadeInDown" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="3"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer3.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc3')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v4" class="col-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="4"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer4.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc4')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v5" class="col-6 col-lg-3 wow fadeInUp" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="5"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer5.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc5')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v6" class="col-6 col-lg-3 wow fadeInRight" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="6"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer7.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc7')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v7" class="col-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="7"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer8.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc8')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v8" class="col-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="8"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer11.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc9')!!}</div>
                        </div>
					</div>
				</a>
			</div>
			<div id="v9" class="col-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s">
				<a href="{{route('ofami.veeracc',[$locale,$type="9"])}}">
					<div class="imgback">
                        <img src="{{asset('images/goveer12.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('veer.acc10')!!}</div>
                        </div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<!--Accessories-->
@endsection