@extends("ofami.main")
@section('content')
<style>
	.container h1{
		font-size:35px;
	}
	.container h2{
		font-size:28px;
	}
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

@if($rentplace=="ofami")
<!--ofami-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="ofami" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 永康租車站 - 樂享學 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb1.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>710台南市永康區中山路298號</p><br>
				<strong>電話</strong>
				<p>06-2028028</p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7488843908427!2d120.25322841496808!3d23.032990684947865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e71d9dec9de13%3A0x9452d692262750ab!2z5qiC5Lqr5a24!5e0!3m2!1szh-TW!2stw!4v1533105789207" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--ofami-->
@endif

@if($rentplace=="anping")
<!--anping-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="anping" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 安平租車站 - 小遊龍 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb2.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>708台南市安平區安北路120號</p><br>
				<strong>電話</strong>
				<p>06-2285472</p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3672.5784012635204!2d120.16212621496744!3d23.002525784963183!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e7602d4a79685%3A0x226cbc17d86c3793!2z5bCP6YGK6b6N6Ieq6KGM6LuK!5e0!3m2!1szh-TW!2stw!4v1533106531507" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--anping-->
@endif

@if($rentplace=="chishang")
<!--chishang-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="chishang" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 池上租車站 - 日光車行 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb3.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>958台東縣池上鄉中正路48號</p><br>
				<strong>電話</strong>
				<p>08-9861117</p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3669.2421679335966!2d121.2185603143362!3d23.12482191830316!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f0c8b3dd31d7b%3A0x6264bf4d8dd4320e!2z5pel5YWJ6LuK6KGM!5e0!3m2!1szh-TW!2stw!4v1533107188959" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--chishang-->
@endif

@if($rentplace=="guanshan")
<!--guanshan-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="guanshan" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 關山租車站 - TR9關山汽車 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb4.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>956台東縣關山鎮博愛路2號</p><br>
				<strong>電話</strong>
				<p>0984-099726</p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d917.8501494521257!2d121.16377862916667!3d23.0457705331335!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346f09f13b6ace07%3A0xb7afc48902196549!2zVFI56Zec5bGx56ef6LuK56uZ44CQ5Y-w5p2x6Zec5bGx54Gr6LuK56uZ56uZ5YWn44CR!5e0!3m2!1szh-TW!2stw!4v1533107378078" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--guanshan-->
@endif

@if($rentplace=="dongshan")
<!--dongshan-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="dongshan" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 冬山租車站 - TR9關山汽車 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb5.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>269宜蘭縣冬山鄉中正路1號</p><br>
				<strong>電話</strong>
				<p>...</p></p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d906.6551139409886!2d121.79147682915253!3d24.636763018032646!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3467e6345fb7d7a7%3A0xcfe81568b0b2a8a8!2z5Yas5bGx54Gr6LuK56uZ!5e0!3m2!1szh-TW!2stw!4v1533107765799" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--dongshan-->
@endif

@if($rentplace=="fulong")
<!--fulong-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="fulong" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 福隆租車站 - 福隆驛站 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb6.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>228新北市貢寮區福隆街2巷</p><br>
				<strong>電話</strong>
				<p>...</p></p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d903.8839062861811!2d121.94462612916911!3d25.015890614295465!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x345d5c5484b55c5b%3A0x50fe76487401c49f!2zTVBGIERyaXZlIOemj-mahuermQ!5e0!3m2!1szh-TW!2stw!4v1533107981254" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--fulong-->
@endif

@if($rentplace=="zengwun")
<!--zengwun-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 id="zengwun" class="heading text-center text-uppercase mb-5" style="font-size:35px;"> 曾文水庫站 - 趣淘漫旅 </h1>
		<div class="row">
			<div align="center">
				<div class="col-sm-2"></div>
				<div class="col-sm-8">
					<img id="chgicon" src="{{asset('images/placeb7.jpg')}}" alt="" class="img-fluid">
				</div>
				<div class="col-sm-2"></div>
				<p><br></p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12" align="left">
				<h2 class="my-2" style="color:#2196F3;">站點介紹</h2><br>
				<p>...</p><br>
				<h2 class="my-2" style="color:#2196F3;">聯絡我們</h2><br>
				<strong>地址</strong>
				<p>715台南市楠西區密枝里密枝102-5號</p><br>
				<strong>電話</strong>
				<p>06-5753333</p></p><br>
			</div>
		</div>
		<a href="{{route('ofami.taiwan')}}" class="btn btn-outline-primary" role="button">　Back　</a>
		<br>
	</div>
</section>
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3666.514256345156!2d120.49875221433803!3d23.224366414585266!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e8b191159fbb1%3A0xf22c8d2104b198a!2z6Laj5reY5ryr5peFIEhPVEVMIENIQU0gQ0hBTSDlj7DljZc!5e0!3m2!1szh-TW!2stw!4v1533108430614" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
<!--zengwun-->
@endif

@endsection