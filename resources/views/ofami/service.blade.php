@extends("ofami.main")
@section('content')

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!-- contact -->
<section class="contact py-5">
	<h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> 聯絡我們 Get In Touch </h1>
	<div class="map">
		<iframe src="" frameborder="0"></iframe>
		<!--
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7488843908445!2d120.2532284143682!3d23.03299068494779!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e71d9dec9de13%3A0x9452d692262750ab!2z5qiC5Lqr5a24!5e0!3m2!1szh-TW!2stw!4v1538549537423"></iframe>-->
		<div class="main_grid_contact">
			<div class="form">
				<h2 class="text-capitalize mb-2 wow fadeInUp" data-wow-duration="2s">歡迎光臨樂享學</h2><br>
				<img class="wow fadeInUp" data-wow-duration="2s" src="{{asset("images/area1.jpg")}}" alt="樂享學,ofami" srcset="">
				<!--
				<form action="#" method="post">
					<div class="input-group">
						<input type="text" class="margin2" name="name" placeholder="first name" required="">
						<input type="text" name="name" placeholder="last name" required="">
					</div>
					<div class="input-group">
						<input type="email" class="margin2" name="email" placeholder="mail@example.com"  required="">
						<input type="text" name="number" placeholder="phone number"  required="">
					</div>
					<div class="input-group">
						<textarea rows="4" cols="50" placeholder="message"></textarea>
					</div>
					<div class="input-group1">
						<input type="submit" value="send">
					</div>
				</form>-->
			</div>
			<div class="w3ls-contact">
				<h3 class="text-capitalize mb-3 wow fadeInUp" data-wow-duration="2s">聯絡資訊</h3>
				<p class="wow fadeInUp" data-wow-duration="2s">樂享學O-Fami，一家結合嬰幼兒產品、電助自行車販賣的複合式咖啡廳，我們希望能夠提供每個孩子及家庭一個舒適、溫馨的聚會享樂空間，同時推廣電助自行車環保、省力又優雅的騎乘經驗。也提供車友們一個舒服的淋浴空間。</p>
				<address>
					<p class="my-3 wow fadeInUp" data-wow-duration="2s"><span class="fas fa-map-marker-alt"></span>710台南市永康區中山路298號 <!--No.298, Zhongshan Rd., Yongkang Dist., Tainan City 710, Taiwan (R.O.C.)--></p>
					<p class="my-3 wow fadeInUp" data-wow-duration="2s"><span class="fas fa-mobile-alt"></span> 06-2028028</p>
					<p class="my-3 wow fadeInUp" data-wow-duration="2s"><span class="fas fa-envelope-open"></span> <a href="mailto:enjlearn@gmail.com">enjlearn@gmail.com</a> </p>
				</address>
				<div class="w3_agileits_social_media mt-3 text-center wow fadeInUp" data-wow-duration="2s">
					<ul>
						<li><a href="https://zh-tw.facebook.com/o.fami.tw/" class="wthree_facebook" target="_blank"><i class="fab fa-facebook-f" aria-hi	dden="true"></i></a></li>
						<li><a href="https://www.instagram.com/o.fami.tw/" class="wthree_dribbble" target="_blank"><i class="fab fa-instagram" aria-hidden="true"></i></a></li>
					</ul>
					<br>
					<div id="fb-root"></div>
					<script>(function(d, s, id) {
						var js, fjs = d.getElementsByTagName(s)[0];
						if (d.getElementById(id)) return;
						js = d.createElement(s); js.id = id;
						js.src = 'https://connect.facebook.net/zh_TW/sdk.js#xfbml=1&version=v3.2';
						fjs.parentNode.insertBefore(js, fjs);
						}(document, 'script', 'facebook-jssdk'));
					</script>
					<div class="fb-like" data-href="https://www.facebook.com/o.fami.tw/" data-layout="button_count" data-action="like" data-size="large" data-show-faces="true" data-share="true"></div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- //contact -->
@endsection