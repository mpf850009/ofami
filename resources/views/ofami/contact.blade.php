@extends("ofami.main")
@section('content')
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->
<div class="main_grid_contact">
	<div class="form">
		<h2 class="text-capitalize mb-2">send us a message</h2>
		<form action="#" method="post">
			<div class="input-group">
				<input type="text" class="margin2" name="name" placeholder="first name" required="">
				<input type="text" name="name" placeholder="last name" required="">
			</div>
			<div class="input-group">
				<input type="email" class="margin2" name="email" placeholder="mail@example.com"  required="">
				<input type="text" name="number" placeholder="phone number"  required="">
			</div>
			<div class="input-group">
				<textarea rows="4" cols="50" placeholder="message"></textarea>
			</div>
			<div class="input-group1">
				<input type="submit" value="send">
			</div>
		</form>
	</div>
	<div class="w3ls-contact">
		<h3 class="text-capitalize mb-3">contact Information</h3>
		<p class="">Maecenas ac euismod eros. Aliquam a suscipit nibh. Aliquam iaculis erat porta mauris fermentum lacinia.</p>
		<address>
			<p class="my-3"><span class="fas fa-map-marker-alt"></span> 1380H 4th cross,<span> Kmome St, NY 10002, Canada.</span> </p>
			<p class="my-3"><span class="fas fa-mobile-alt"></span> +14 999 888 7773 </p>
			<p class="my-3"><span class="fas fa-envelope-open"></span> <a href="mailto:mail@example.com">mail@example.com</a> </p>
		</address>
		<div class="w3_agileits_social_media mt-3 text-center">
			<ul>
				<li><a href="#" class="wthree_facebook"><i class="fab fa-facebook-f" aria-hidden="true"></i></a></li>
				<li><a href="#" class="wthree_twitter"><i class="fab fa-twitter" aria-hidden="true"></i></a></li>
				<li><a href="#" class="wthree_dribbble"><i class="fab fa-dribbble" aria-hidden="true"></i></a></li>
				<li><a href="#" class="wthree_behance"><i class="fab fa-behance" aria-hidden="true"></i></a></li>
			</ul>
		</div>
	</div>
</div>

@endsection