<div class="card-header wow fadeInUp" data-wow-duration="2s">快速瀏覽</div>
<div class="card-body wow fadeInUp" data-wow-duration="2s">
    <a @if ($section=='discount') class="hvr-icon-wobble-horizontal active1 active2" @endif class="hvr-icon-wobble-horizontal active1" href="{{route("ofami.bikerental",[$locale,$nav="discount"])}}">
        <h5 class="my-1">優惠活動</h5>
    </a>
    <!--
    <a @if ($section=='price') class="hvr-icon-wobble-horizontal active1 active2" @endif class="hvr-icon-wobble-horizontal active1"  href="{{route("ofami.bikerental",[$locale,$nav="price"])}}">
        <h5 class="my-1">車款價目</h5>
    </a>-->
    <a @if ($section=='rundown') class="hvr-icon-wobble-horizontal active1 active2" @endif class="hvr-icon-wobble-horizontal active1" href="{{route("ofami.bikerental",[$locale,$nav="rundown"])}}">
        <h5 class="my-1">租車流程</h5>
    </a>
    <a @if ($section=='guide') class="hvr-icon-wobble-horizontal active1 active2" @endif class="hvr-icon-wobble-horizontal active1" href="{{route("ofami.bikerental",[$locale,$nav="guide"])}}">
        <h5 class="my-1">專業導覽</h5>
    </a>    
    <!--
    <a @if ($section=='guide1'||$section=='hm'||$section=='wst'||$section=='xh'||$section=='ap'||$section=='ysrtsip') class="hvr-icon-wobble-horizontal active1 active2" @endif class="hvr-icon-wobble-horizontal active1" href="{{route("ofami.bikerental",[$locale,$nav="guide1"])}}">
        <h5 class="my-1">更新導覽</h5>
    </a>
    <a @if ($section=='reservation') class="hvr-icon-wobble-horizontal active1 active2" @endif class="hvr-icon-wobble-horizontal active1" target="_blank" href="http://www.mpfinside.com/MPF_Rent_Server/order_index.php">
        <h5 class="my-1">我要預約</h5>
    </a>-->
</div>