@extends("ofami.main")
@section('content')

<script type="text/javascript" src="{{asset("js/map/mapdata.js")}}"></script>		
<script  type="text/javascript" src="{{asset("js/map/countrymap.js")}}"></script>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->

<div class="row">
    <div class="col-md-2"></div>
    <div class="col-md-6" id="map"></div>
    <div class="col-md-4"></div>
</div>

<!--
<div class="col-md-6"><img class="img-responsive" src="images/place1.jpg" alt="" srcset=""></div>
-->

@endsection