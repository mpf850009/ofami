@extends("ofami.main")
@section('content')
<style>
	.container h1{
		font-size:35px;
	}
	.wrap_view h3{
		font-size:35px;
	}
	.port-info h5,p{
		text-align: center;
	}
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->
<!-- food gallery -->
<section class="banner_bottom proj py-5 my-lg-5">
	<div class="wrap_view">
		<h3 class="heading text-center text-uppercase mb-5 wow fadeInUp"> MENU 菜單 </h3>
		<div class="inner_sec">
			<ul class="portfolio-categ filter">
				<li class="port-filter all active">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s">{{trans('cafe.all')}}</a>
				</li>
				<li class="cat-item-1">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.snack')}}">{{trans('cafe.snack')}}</a>
				</li>
				<li class="cat-item-2">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.waffle')}}">{{trans('cafe.waffle')}}</a>
				</li>
				<li class="cat-item-3">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.brunch')}}">{{trans('cafe.brunch')}}</a>
				</li>
				<li class="cat-item-4">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.sd')}}">{{trans('cafe.sd')}}</a>
				</li>
				<li class="cat-item-5">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.tea')}}">{{trans('cafe.tea')}}</a>
				</li>
				<li class="cat-item-6">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.it')}}">{{trans('cafe.it')}}</a>
				</li>
				<li class="cat-item-7">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.cafe')}}">{{trans('cafe.cafe')}}</a>
				</li>
				<li class="cat-item-8">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.smoothie')}}">{{trans('cafe.smoothie')}}</a>
				</li>
				<li class="cat-item-9">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.soda')}}">{{trans('cafe.soda')}}</a>
				</li>
				<li class="cat-item-10">
					<a href="#" class="wow fadeInUp" data-wow-duration="2s" title="{{trans('cafe.secret')}}">{{trans('cafe.secret')}}</a>
				</li>
			</ul>
			<ul class="portfolio-area">
				<li class="portfolio-item2 mb-2 " data-id="id-1" data-type="cat-item-1">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/ice2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/ice2.jpg')}}" class="img-responsive" alt="冰淇淋歡樂派對">
								<div class="port-info">
									<h5>冰淇淋歡樂派對<br>NTD.120</h5>
									<p>Ice Cream Party<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-2" data-type="cat-item-1">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/ice1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/ice1.jpg')}}" class="img-responsive" alt="濃縮冰淇淋">
								<div class="port-info">
									<h5>濃縮冰淇淋<br>NTD.90</h5>
									<p>Espresso Ice Cream<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-3" data-type="cat-item-1">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/ice4b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/ice4.jpg')}}" class="img-responsive" alt="鮮奶酪">
								<div class="port-info">
									<h5>鮮奶酪<br>NTD.45</h5>
									<p>Panna Cotta<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-4" data-type="cat-item-1">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/ice3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/ice3.jpg')}}" class="img-responsive" alt="焗烤馬鈴薯">
								<div class="port-info">
									<h5>焗烤馬鈴薯<br>NTD.130</h5>
									<p>Ice Cream Party<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-5" data-type="cat-item-2">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/waffle1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/waffle1.jpg')}}" class="img-responsive" alt="愛抹茶請選我">
								<div class="port-info">
									<h5>愛抹茶請選我<br>NTD.190</h5>
									<p>Matcha&Red Bean Waffle<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-6" data-type="cat-item-2">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/waffle2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/waffle2.jpg')}}" class="img-responsive" alt="甜食控派對">
								<div class="port-info">
									<h5>甜食控派對<br>NTD.180</h5>
									<p>Banana Chocolate Waffle<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-7" data-type="cat-item-2">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/waffle3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/waffle3.jpg')}}" class="img-responsive" alt="鮮果小鳥胃">
								<div class="port-info">
									<h5>鮮果小鳥胃<br>NTD.170</h5>
									<p>Fresh Fruit Waffle<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-8" data-type="cat-item-3">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/toast1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/toast1.jpg')}}" class="img-responsive" alt="樂享挪威燻鮭魚焗吐司">
								<div class="port-info">
									<h5>樂享挪威燻鮭魚焗吐司<br>NTD.300</h5>
									<p>Cheese on top toast with smoked salmon & Veggies　　　</p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-9" data-type="cat-item-3">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/toast2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/toast2.jpg')}}" class="img-responsive" alt="檸香魚來樂焗吐司">
								<div class="port-info">
									<h5>檸香魚來樂焗吐司<br>NTD.300</h5>
									<p>Cheese on top toast with Fresh Fish and Veggies　　　<br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-10" data-type="cat-item-3">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/toast3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/toast3.jpg')}}" class="img-responsive" alt="樂享學經典早午餐">
								<div class="port-info">
									<h5>樂享學經典早午餐<br>NTD.300</h5>
									<p>Bread with Sausage and Cheese Omelette and Veggies　　　</p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-11" data-type="cat-item-3">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/toast4b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/toast4.jpg')}}" class="img-responsive" alt="阿瓦卡多焗吐司">
								<div class="port-info">
									<h5>阿瓦卡多焗吐司<br>NTD.290</h5>
									<p>Cheese on top toast with Avocado &Veggies<br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-12" data-type="cat-item-3">
					<div>
						<span class="image-block img-hover">	
							<a class="image-zoom" href="{{asset('images/toast5b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/toast5.jpg')}}" class="img-responsive" alt="菇蛋不孤單">
								<div class="port-info">
									<h5>菇蛋不孤單<br>NTD.280</h5>
									<p>Bread with Mushrooms & Fried Eggs & Veggies<br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-13" data-type="cat-item-3">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/toast6b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/toast6.jpg')}}" class="img-responsive" alt="慢火焗烤燻雞吐司">
								<div class="port-info">
									<h5>慢火焗烤燻雞吐司<br>NTD.270</h5>
									<p>Cheese on top toast with smoked chicken & Veggies　　　</p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-14" data-type="cat-item-4">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/special1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/special1.jpg')}}" class="img-responsive" alt="嗡嗡檸檬">
								<div class="port-info">
									<h5>嗡嗡檸檬<br>NTD.90</h5>
									<p>Honey Lemon<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-15" data-type="cat-item-4">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/special4b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/special4.jpg')}}" class="img-responsive" alt="甜甜朱古力">
								<div class="port-info">
									<h5>甜甜朱古力<br>NTD.100</h5>
									<p>Cocoa Latte<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-43" data-type="cat-item-4">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/special5b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/special5.jpg')}}" class="img-responsive" alt="季節果汁">
								<div class="port-info">
									<h5>季節果汁<br>NTD.100</h5>
									<p>Season Juice<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-16" data-type="cat-item-4">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/special2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/special2.jpg')}}" class="img-responsive" alt="抹茶牛奶">
								<div class="port-info">
									<h5>抹茶牛奶<br>NTD.120</h5>
									<p>Matcha Latte<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-17" data-type="cat-item-4">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/special3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/special3.jpg')}}" class="img-responsive" alt="藍色天空">
								<div class="port-info">
									<h5>藍色天空<br>NTD.130</h5>
									<p>Mulberry Milk<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-18" data-type="cat-item-5">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/tea1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/tea1.jpg')}}" class="img-responsive" alt="大正紅茶">
								<div class="port-info">
									<h5>大正紅茶<br>NTD.60</h5>
									<p>Black Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-19" data-type="cat-item-5">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/tea2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/tea2.jpg')}}" class="img-responsive" alt="白毫綠茶">
								<div class="port-info">
									<h5>白毫綠茶<br>NTD.60</h5>
									<p>Green Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-20" data-type="cat-item-5">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/tea6b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/tea6.jpg')}}" class="img-responsive" alt="鐵觀音烏龍茶">
								<div class="port-info">
									<h5>鐵觀音烏龍茶<br>NTD.60</h5>
									<p>Oolong Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-21" data-type="cat-item-5">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/tea3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/tea3.jpg')}}" class="img-responsive" alt="農民鮮奶紅茶">
								<div class="port-info">
									<h5>農民鮮奶紅茶<br>NTD.80</h5>
									<p>Fresh Milk Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-22" data-type="cat-item-5">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/tea4b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/tea4.jpg')}}" class="img-responsive" alt="歐巴柚子茶">
								<div class="port-info">
									<h5>歐巴柚子茶<br>NTD.80</h5>
									<p>Citron Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-23" data-type="cat-item-5">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/tea5b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/tea5.jpg')}}" class="img-responsive" alt="漂浮冰淇林紅茶">
								<div class="port-info">
									<h5>漂浮冰淇林紅茶<br>NTD.90</h5>
									<p>Ice Cream Black Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-24" data-type="cat-item-6">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coldtea1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coldtea1.jpg')}}" class="img-responsive" alt="蜜香紅茶">
								<div class="port-info">
									<h5>蜜香紅茶<br>NTD.60</h5>
									<p>Assam Black Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-25" data-type="cat-item-6">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coldtea2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coldtea2.jpg')}}" class="img-responsive" alt="玄米綠茶">
								<div class="port-info">
									<h5>玄米綠茶<br>NTD.60</h5>
									<p>Brown Rice Green Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-26" data-type="cat-item-6">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coldtea3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coldtea3.jpg')}}" class="img-responsive" alt="高山青茶">
								<div class="port-info">
									<h5>高山青茶<br>NTD.60</h5>
									<p>Taiwan Green Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-27" data-type="cat-item-6">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coldtea4b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coldtea4.jpg')}}" class="img-responsive" alt="桂花烏龍">
								<div class="port-info">
									<h5>桂花烏龍<br>NTD.60</h5>
									<p>Osmanthus Oolong Tea<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-28" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee1.jpg')}}" class="img-responsive" alt="義式濃縮咖啡">
								<div class="port-info">
									<h5>義式濃縮咖啡<br>NTD.70</h5>
									<p>Espresso<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-30" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee5b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee5.jpg')}}" class="img-responsive" alt="義式黑咖啡(熱)">
								<div class="port-info">
									<h5>美式黑咖啡<br>NTD.90</h5>
									<p>Caffé Americano<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-45" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee8b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee8.jpg')}}" class="img-responsive" alt="卡布奇諾">
								<div class="port-info">
									<h5>卡布奇諾<br>NTD.130</h5>
									<p>Cappuccino<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-32" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee7b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee7.jpg')}}" class="img-responsive" alt="拿鐵咖啡(熱)">
								<div class="port-info">
									<h5>拿鐵咖啡<br>NTD.130</h5>
									<p>Caffé Latte<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-33" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee2.jpg')}}" class="img-responsive" alt="香檸拿鐵">
								<div class="port-info">
									<h5>香檸拿鐵<br>NTD.130</h5>
									<p>Lemon Cappuccino<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-34" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee3.jpg')}}" class="img-responsive" alt="漂浮冰拿鐵">
								<div class="port-info">
									<h5>漂浮冰拿鐵<br>NTD.130</h5>
									<p>Ice Cream Café Latte<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-44" data-type="cat-item-7">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/coffee9b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/coffee9.jpg')}}" class="img-responsive" alt="抹茶拿鐵咖啡">
								<div class="port-info">
									<h5>抹茶拿鐵咖啡<br>NTD.130</h5>
									<p>Ice Matcha Caffe Latte　　<br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-35" data-type="cat-item-8">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/smoothie5b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/smoothie5.jpg')}}" class="img-responsive" alt="繽紛檸檬冰沙">
								<div class="port-info">
									<h5>繽紛檸檬冰沙<br>NTD.110</h5>
									<p>Lemon Smoothie<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-36" data-type="cat-item-8">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/smoothie4b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/smoothie4.jpg')}}" class="img-responsive" alt="義式咖啡冰沙">
								<div class="port-info">
									<h5>義式咖啡冰沙<br>NTD.120</h5>
									<p>Espresso Smoothie<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-37" data-type="cat-item-8">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/smoothie3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/smoothie3.jpg')}}" class="img-responsive" alt="抹茶豆漿冰沙">
								<div class="port-info">
									<h5>抹茶豆漿冰沙<br>NTD.130</h5>
									<p>Matcha SoyMilk Smoothie<br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-38" data-type="cat-item-8">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/smoothie2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/smoothie2.jpg')}}" class="img-responsive" alt="仲夏野莓冰沙">
								<div class="port-info">
									<h5>仲夏野莓冰沙<br>NTD.130</h5>
									<p>WildBerry Smoothie<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-39" data-type="cat-item-8">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/smoothie1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/smoothie1.jpg')}}" class="img-responsive" alt="巧克力冒險工廠">
								<div class="port-info">
									<h5>巧克力冒險工廠<br>NTD.130</h5>
									<p>ChocolateMilk Smoothie<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-40" data-type="cat-item-9">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/soda1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/soda1.jpg')}}" class="img-responsive" alt="">
								<div class="port-info">
									<h5>可樂<br>NTD.40</h5>
									<p>Coke<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-41" data-type="cat-item-9">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/soda2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/soda2.jpg')}}" class="img-responsive" alt="">
								<div class="port-info">
									<h5>雪碧<br>NTD.40</h5>
									<p>Sprite<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-42" data-type="cat-item-9">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/soda3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/soda3.jpg')}}" class="img-responsive" alt="">
								<div class="port-info">
									<h5>氣泡水<br>NTD.40</h5>
									<p>Perrier<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-42" data-type="cat-item-10">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/secret1b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/secret1.jpg')}}" class="img-responsive" alt="">
								<div class="port-info">
									<h5>大人的雞腿飯<br>NTD.280</h5>
									<p>Adult&apos;s Drumstick rice<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-42" data-type="cat-item-10">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/secret2b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/secret2.jpg')}}" class="img-responsive" alt="">
								<div class="port-info">
									<h5>草莓鬆餅<br>NTD.120</h5>
									<p>Strawberry Muffins<br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
				<li class="portfolio-item2 mb-2" data-id="id-42" data-type="cat-item-10">
					<div>
						<span class="image-block img-hover">
							<a class="image-zoom" href="{{asset('images/secret3b.jpg')}}" rel="prettyPhoto[gallery]">
								<img src="{{asset('images/secret3.jpg')}}" class="img-responsive" alt="">
								<div class="port-info">
									<h5>摩卡咖啡<br>NTD.130</h5>
									<p>Café Mocha <br><br><br></p>
								</div>
							</a>
						</span>
					</div>
				</li>
			</ul>
			<!--end portfolio-area -->
			<div class="clearfix"></div>
		</div>
	</div>
</section>
<!--food gallery-->
@endsection