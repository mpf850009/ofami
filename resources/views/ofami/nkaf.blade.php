@extends("ofami.main")
@section('content')
@include('ofami.script.nikimotionscript')
@include("ofami.css.imghover")
<style>
	.container h1{
		font-size:35px;
	}
	.container h2{
		font-size:28px;
	}
	.row ul{
		list-style-image:url('../images/nklogo3.png');
		line-height:30px;
	}
	.row strong{
		font-size: 20px;
	}
	.imgcircle img{
		border-radius: 50%;
		width:30%;
	}
	.price{
		font-family:price;
		font-size: 30px;
		float: right;
	}
	.imgcircle {
		font-size:10px;
	}
	.pad_0{
		padding:0;
	}
	.marign_top{
		margin-top:5px;
	}
	@media (max-width:460px) {
		.col-xs-3 h4 {
			font-size:20px;
			padding:0 0em;
			
		margin-top:2px;
		}
	}
	@media (max-width:396px) {
		.col-xs-3 h4 {
			font-size:20px;
			padding:0 1em;
		}
	}
	.col-3 {
		font-size:20px;
		padding:0 ;
	}
	.col-xs-3 {
		font-size:20px;
		padding:0 ;
	}
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--Autofold video-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 id="affc" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s" style="font-size:35px;"> Autofold </h1>
		<iframe class="wow jackInTheBox" data-wow-duration="2s" width="100%" height="500" src="https://www.youtube.com/embed/p1vpCyCxLbs?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</section>
<!--Autofold video-->

<!--Autofold product-->
<!-- <section class="work py-5 my-lg-5">
	<div class="container">
		<h1 id="affc" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s" style="font-size:35px;"> Product 產品</h1>
		<div class="row">
			<div class="col-sm-6" align="center">
				<div class="row wow fadeInUp" data-wow-duration="2s">
					<div class="col-2">
						<a class="btnFollow8" title="color-Scarlett">
							<div class="imgcircle"><img src="{{asset('images/color-Scarlett.png')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow1" title="color-Berry">
							<div class="imgcircle"><img src="{{asset('images/color-Berry.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow2" title="color-Capri">
							<div class="imgcircle"><img src="{{asset('images/color-Capri.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow3" title="color-Ebony">
							<div class="imgcircle"><img src="{{asset('images/color-Ebony.png')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow4" title="color-Emerald">
							<div class="imgcircle"><img src="{{asset('images/color-Emerald.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow5" title="color-Mink">
							<div class="imgcircle"><img src="{{asset('images/color-Mink.jpg')}}"></div>
						</a>
					</div>
				</div>
				<p><br></p>
				<div class="row wow fadeInUp" data-wow-duration="2s">
					<div class="col-2">
						<a class="btnFollow6" title="color-Oyster">
							<div class="imgcircle"><img src="{{asset('images/color-Oyster.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow7" title="color-Tangerine">
							<div class="imgcircle"><img src="{{asset('images/color-Tangerine.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow9" title="color-Forest">
							<div class="imgcircle"><img src="{{asset('images/color-Forest.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow10" title="color-Graphite">
							<div class="imgcircle"><img src="{{asset('images/color-Graphite.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow11" title="color-GreyBlue">
							<div class="imgcircle"><img src="{{asset('images/color-GreyBlue.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="btnFollow12" title="color-Camel">
							<div class="imgcircle"><img src="{{asset('images/color-Camel.jpg')}}"></div>
						</a>
					</div>
				</div>
				<div class="col-sm-1"></div>
				<div class="col-sm-10">
					<div class="col-sm-12">
						<div class="imgback">
							<a id="chghref" class="image-zoom wow fadeInUp" data-wow-duration="2s" href="{{asset('images/afgif1.gif')}}" rel="prettyPhoto[gallery]">
								<img id="chgicon" src="{{asset('images/afgif1.gif')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,autofold">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</a>
						</div>
					</div>
				</div>
				<div class="col-sm-1"></div> -->
				<!--<p class="price">NTD. 10,999　</p>--><br>
				<!-- <a href="http://www.pcstore.com.tw/ofami/S01JFPB.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button" style="float:right;"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<div class="clearfix"></div>
				<p><br></p>
				<p><br></p>
			</div>
			<div class="col-sm-6 wow fadeInUp" data-wow-duration="2s" align="left" style="padding:0em 4em;">
				{!!trans('nikimotion.af')!!}
			</div>
		</div>
	</div>
</section> -->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 id="affc" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s" style="font-size:35px;"> Product 產品</h1>
		<div class="row">
			<div class="col-lg-6" align="center" >
				<div class="col-12">
					<div class="imgback">
						<a id="chghref" class="image-zoom wow fadeInUp" data-wow-duration="2s" href="{{asset('images/afgif1.gif')}}" rel="prettyPhoto[gallery]">
							<img id="chgicon" src="{{asset('images/afgif1.gif')}}" class="img-responsive" style=" width: 80%;"alt="樂享學,ofami,嬰兒車,nikimotion,autofold">
							<div class="overlay">
								<div class="text">
									<span class="fas fa-search"></span>
								</div>
							</div>
						</a>
					</div>
				</div><br>	
				<div class="row col-12">
					<div class="col-3 col-xs-3 marign_top " >
						<h4>運動系列</h4>
					</div>
					<div class="col-9  col-xs-9 ">
						<div class="row wow fadeInUp col-12" data-wow-duration="2s">
							<div class="col-4 pad_0">
								<a class="btnFollow8" title="color-Scarlett">
									<div class="imgcircle" width="40px" height="40px"><img src="{{asset('images/color-Scarlett.png')}}" ></div>
									<p>珊瑚紅</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow1" title="color-Berry">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Berry.jpg')}}"></div>
									<p>紅莓紫</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow2" title="color-Capri">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Capri.jpg')}}"></div>
									<p>湖水藍</p>
								</a>
							</div>
						</div><br>
						<div class="row wow fadeInUp col-12 " data-wow-duration="2s">
							<div class="col-4 pad_0 ">
								<a class="btnFollow7" title="color-Tangerine">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Tangerine.jpg')}}"></div>
									<p>橙陽橘</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow4" title="color-Emerald">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Emerald.jpg')}}"></div>
									<p>大地綠</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow13" title="color-black">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-black.png')}}"></div>
									<p>經典黑</p>
								</a>
							</div>
						</div><br>
					</div>
				</div>
				 <div class="row col-12">
					<div class="col-3 col-xs-3 marign_top">
						<h4>亞麻系列</h4>
					</div>	
					<div class="col-9 col-xs-9">
						<div class="row wow fadeInUp col-12" data-wow-duration="2s">
							<div class="col-4 pad_0 ">
								<a class="btnFollow14" title="color-Forest">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Forest.jpg')}}"></div>
									<p>森林綠</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow15" title="color-Graphite">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Graphite.jpg')}}"></div>
									<p>石墨黑</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow16" title="color-GreyBlue">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-GreyBlue.jpg')}}"></div>
									<p>天藍灰</P>
								</a>
							</div>
						</div><br>
						<div class="row wow fadeInUp col-12" data-wow-duration="2s">
							<div class="col-4 pad_0">
								<a class="btnFollow17" title="color-Camel">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Camel.jpg')}}"></div>
									<p>卡其駝</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow18" title="color-Cherry">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Cherry.jpg')}}"></div>
									<p>櫻桃紅</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow19" title="color-Aqua">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Aqua.jpg')}}"></div>
									<p>天空藍</p>
								</a>
							</div>
						</div><br>
					</div>	
				</div> 
				<div class="row col-12">
					<div class="col-3 col-xs-3 marign_top">
						<h4>經典系列</h4>
					</div>
					<div class="col-9 col-xs-9 ">
						<div class="row wow fadeInUp col-12" data-wow-duration="2s">
							<div class="col-4 pad_0">
								<a class="btnFollow20" title="color-Mink">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Mink.jpg')}}"></div>
									<p>米白貂</p>
								</a>
							</div><br>
							<div class="col-4 pad_0 ">
								<a class="btnFollow21" title="color-Oyster">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Oyster.jpg')}}"></div>
									<p>牡蠣白</p>
								</a>
							</div><br>
							<div class="col-4 pad_0">
								<a class="btnFollow22" title="color-Ebony">
									<div class="imgcircle"width="40px" height="40px"><img src="{{asset('images/color-Ebony.png')}}"></div>
									<p>檀木藍</p>
								</a>
							</div>
						</div>
						<p><br></p>
					</div>
				</div> 
			</div>
			<div class="col-lg-6" style="margin-top:50px;">
				<div class="col-12 wow fadeInUp" data-wow-duration="2s" align="left" style="padding:0 4em;">
					{!!trans('nikimotion.af')!!}
				</div>
			</div>
		</div>
	</div>	
</section>		
<!--autofold product-->
<!--  autofold picture-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<div class="col-12 ">
			<img src="{{asset('images/au0.png')}}">
		</div>
		<div class="col-12 ">
			<img src="{{asset('images/au2.png')}}">
		</div>
		<div class="col-12 ">
			<img src="{{asset('images/au3.png')}}">
		</div>
		<div class="col-12 ">
			<img src="{{asset('images/au4.png')}}">
		</div>
		<div class="col-12 ">
			<img src="{{asset('images/au5.png')}}">
		</div>
		<div class="col-12 ">
			<img src="{{asset('images/au6.png')}}">
		</div>
		<div class="col-12 ">
			<img src="{{asset('images/au8.png')}}">
		</div>
	</div>
</section>
<!--Autofold accessory-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 id="affc" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s" style="font-size:35px;"> Autofold Accessory 配件</h1>
		<div class="row">
			<div class="col-12" align="left">
				<div class="row">
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#nkac1">
							<div class="imgback">
								<img src="{{asset('images/nkac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,autofold,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ch')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#afac1">
							<div class="imgback">
								<img src="{{asset('images/afac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.afa')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#afac2">
							<div class="imgback">
								<img src="{{asset('images/afac2.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.mn')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#afac3">
							<div class="imgback">
								<img src="{{asset('images/afac3.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.rc')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#blac5">
							<div class="imgback">
								<img src="{{asset('images/blac5.png')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ch4')}}</div>
								</div>
							</div>
						</a>
					</div>
				</div><p><br></p>
			</div>
		</div>
		<a href="{{route('ofami.nikimotion',$locale)}}#af" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>
		<br>
	</div>
</section>
<!--autofold accessory-->

@endsection