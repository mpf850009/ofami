@extends("ofami.main")
@section('content')
@include('ofami.script.nikimotionscript')
@include("ofami.css.imghover")
<style>
	.container h1{
		font-size:35px;
	}
	.container h2{
		font-size:28px;
	}
	.row ul{
		list-style-image:url('../images/nklogo3.png');
		line-height:40px;
	}
	.row strong{
		font-size: 20px;
	}
	.imgcircle img{
		border-radius: 50%;
		margin: 10px 10px;
		padding: 12px 12px;
	}
	.price{
		font-family:price;
		font-size: 30px;
		float: right;
	}
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--Autofoldlite-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Autofold lite </h1>
		<iframe class="wow jackInTheBox" data-wow-duration="2s" width="100%" height="500" src="https://www.youtube.com/embed/p1vpCyCxLbs?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</section>
<!--autofoldlite-->

<!--Autofoldlite product-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Product 產品</h1>
		<div class="row">
			<div id="aflfc" class="col-sm-6" align="center">
				<div class="row wow fadeInUp" data-wow-duration="2s">
					<div class="col-3"></div>
					<div class="col-3 col-lg-2">
						<a class="color1" title="color-Green">
							<div class="imgcircle"><img src="{{asset('images/color-green.png')}}"></div>
						</a>
					</div>
					<div class="col-3 col-lg-2">
						<a class="color2" title="color-Red">
							<div class="imgcircle"><img src="{{asset('images/color-red.jpg')}}"></div>
						</a>
					</div>
					<div class="col-3 col-lg-2">
						<a class="color3" title="color-Black">
							<div class="imgcircle"><img src="{{asset('images/color-black.png')}}"></div>
						</a>
					</div>
					<div class="col-3"></div>
				</div>
				<p><br></p>
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="imgback">
						<a id="aflhref" class="image-zoom wow fadeInUp" data-wow-duration="2s" href="{{asset('images/aflgif1.gif')}}" rel="prettyPhoto[gallery]">
							<img id="aflicon" src="{{asset('images/aflgif1.gif')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,autofold,lite">
							<div class="overlay">
								<div class="text">
									<span class="fas fa-search"></span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<!--
				<p class="price">NTD. 8,999　</p>--><br>
				<a href="http://www.pcstore.com.tw/ofami/S01LHHM.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button" style="float:right;"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<div class="clearfix"></div>
				<p><br></p>
			</div>
			<div class="col-sm-6 wow fadeInUp" data-wow-duration="2s" align="left" style="padding:0em 3.5em;">
				{!!trans('nikimotion.afl')!!}
			</div>
		</div>
	</div>
</section>
<!--autofoldlite product-->

<!--Autofoldlite accessory-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Autofold lite Accessory 配件</h1>
		<div class="row">
			<div class="col-12" align="left">
				<div class="row">
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#nkac1">
							<div class="imgback">
								<img src="{{asset('images/nkac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,autofold,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ch')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#afac1">
							<div class="imgback">
								<img src="{{asset('images/afac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.afa')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#afac2">
							<div class="imgback">
								<img src="{{asset('images/afac2.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.mn')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#afac3">
							<div class="imgback">
								<img src="{{asset('images/afac3.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.rc')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#aflac1">
							<div class="imgback">
								<img src="{{asset('images/aflac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ch2')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-3">
						<a class="wow fadeInUp" data-wow-duration="2s" href="{{route('ofami.nkacc',$locale)}}#blac5">
							<div class="imgback">
								<img src="{{asset('images/blac5.png')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ch4')}}</div>
								</div>
							</div>
						</a>
					</div>
				</div><p><br></p>
			</div>
		</div>
		<a href="{{route('ofami.nikimotion',$locale)}}#afl" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>
		<br>
	</div>
</section>
<!--autofoldlite accessory-->

@endsection