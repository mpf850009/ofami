@extends("ofami.main")
@section('content')
@include('ofami.script.nikimotionscript')
@include("ofami.css.imghover")
<style>
	.container h1{
		font-size:35px;
	}
	.container h2{
		font-size:28px;
	}
	.row ul{
		list-style-image:url('../images/nklogo3.png');
		line-height:40px;
	}
	.row strong{
		font-size: 20px;
	}
	.imgcircle img{
		border-radius: 50%;
	}
	.price{
		font-family:price;
		font-size: 30px;
		float: right;
	}
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--Blade-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Blade </h1>
		<iframe class="wow jackInTheBox" data-wow-duration="2s" width="100%" height="500" src="https://www.youtube.com/embed/Me_FjvwhipI?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</section>
<!--blade-->

<!--Blade product-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Product 產品</h1>
		<div class="row">
			<div id="blfc" class="col-sm-6" align="center">
				<div class="row wow fadeInUp" data-wow-duration="2s">
					<div class="col-2">
						<a class="bladec1" title="color-Capri">
							<div class="imgcircle"><img src="{{asset('images/color-Capri.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="bladec2" title="color-Berry">
							<div class="imgcircle"><img src="{{asset('images/color-Berry.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="bladec3" title="color-Ebony">
							<div class="imgcircle"><img src="{{asset('images/color-Ebony.png')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="bladec4" title="color-Emerald">
							<div class="imgcircle"><img src="{{asset('images/color-Emerald.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="bladec5" title="color-Mink">
							<div class="imgcircle"><img src="{{asset('images/color-Mink.jpg')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="bladec6" title="color-Oyster">
							<div class="imgcircle"><img src="{{asset('images/color-Oyster.jpg')}}"></div>
						</a>
					</div>
				</div>
				<p><br></p>
				<div class="row wow fadeInUp" data-wow-duration="2s">
					<div class="col-2">
						<a class="bladec7" title="color-Scarlett">
							<div class="imgcircle"><img src="{{asset('images/color-Scarlett.png')}}"></div>
						</a>
					</div>
					<div class="col-2">
						<a class="bladec8" title="color-Tangerine">
							<div class="imgcircle"><img src="{{asset('images/color-Tangerine.jpg')}}"></div>
						</a>
					</div>
				</div><p><br></p>
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="imgback">
						<a id="bladehref" class="image-zoom wow fadeInUp" data-wow-duration="2s" href="{{asset('images/bladegif1.gif')}}" rel="prettyPhoto[gallery]">
							<img id="bladeicon" src="{{asset('images/bladegif1.gif')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,blade,nikimotion">
							<div class="overlay">
								<div class="text">
									<span class="fas fa-search"></span>
								</div>
							</div>
						</a>
					</div>
				</div>
				<div class="col-md-2"></div>
				<!--
				<p class="price">NTD. 31,999</p>-->
				<div class="clearfix"></div>
				<p><br></p>
			</div>
			<div class="col-sm-6 wow fadeInUp" data-wow-duration="2s" align="left" style="padding:0em 4em;">
				{!!trans('nikimotion.blade')!!}
			</div>
		</div>
	</div>
</section>
<!--Blade product-->

<!--Blade Accessory-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Blade Accessory 配件</h1>
		<div class="row">
			<div class="col-12" align="left">
				<div class="row">
					<div class="col-lg-4"></div>
					<div class="col-6 col-lg-2">
						<a href="{{route('ofami.nkacc',$locale)}}#nkac1">
							<div class="imgback">
								<img src="{{asset('images/nkac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid wow fadeInUp" data-wow-duration="2s">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ch')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-6 col-lg-2">
						<a href="{{route('ofami.nkacc',$locale)}}#blac1">
							<div class="imgback">
								<img src="{{asset('images/blac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid wow fadeInUp" data-wow-duration="2s">
								<div class="overlay">
									<div class="text">{{trans('nikimotion.ba')}}</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-4"></div>
				</div>
				<p><br></p>
			</div>
		</div>	
		<a href="{{route('ofami.nikimotion',$locale)}}#blade" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>
		<br>
	</div>
</section>
<!--Blade Accessory-->

@endsection