@extends("ofami.main")
@section('content')
@include("ofami.css.imghover")
@include('ofami.script.veercript')
@include('ofami.script.nikimotionscript')
@include('ofami.script.camoscript')
<link rel="stylesheet" href="{{asset('css/flexslider/flexslider.css')}}" type="text/css">
<style>
	* {font-family: 'su1';}
	.container h1{
		font-size:35px;
	}
	.row h2{
		font-size:28px;
		line-height: 40px;
	}
	.price{
		font-family:price1;
		font-size: 30px;
		float: right;
	}
	.price1{
		font-family:price1;
		font-size: 20px;
	}
	.row ol{
		line-height: 35px;
	}
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--Accessories-->
<section class="work py-5 my-lg-3">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s">Accessories 周邊配件 </h1>
		
		@if($type=="1")
		<div class="work py-5 my-lg-5">
				<div class="container">
               		<iframe width="100%" height="500" src="https://www.youtube.com/embed/ntr320rlD48?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<div class="clearfix"></div><br>
				</div>
			</div>
			<div class="row" id="veergif4">
				<div class="col-sm-6" align="center">
					<div class="col-lg-1"></div>
					<div class="col-lg-10">
						<div class="col-sm-12 flexslider">
							<!--
							<a class="image-zoom" href="{{asset('images/veergif4.gif')}}" rel="prettyPhoto[gallery]">
								<div class="imgback">
									<img src="{{asset('images/veergif4.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
									<div class="overlay">
										<div class="text">
											<span class="fas fa-search"></span>
										</div>
									</div>
								</div>
							</a>-->
							<ul class="slides wow fadeInUp" data-wow-duration="2s">
								<li data-thumb="{{asset('images/veeracc11.jpg')}}">
									<a class="image-zoom" href="{{asset('images/veeracc11b.jpg')}}" rel="prettyPhoto[gallery]">
										<div class="imgback">
											<img src="{{asset('images/veeracc11a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
											<div class="overlay">
												<div class="text">
													<span class="fas fa-search"></span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li data-thumb="{{asset('images/veeracc12.jpg')}}">
									<a class="image-zoom" href="{{asset('images/veeracc12b.jpg')}}" rel="prettyPhoto[gallery]">
										<div class="imgback">
											<img src="{{asset('images/veeracc12a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
											<div class="overlay">
												<div class="text">
													<span class="fas fa-search"></span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li data-thumb="{{asset('images/veeracc13.jpg')}}">
									<a class="image-zoom" href="{{asset('images/veeracc13b.jpg')}}" rel="prettyPhoto[gallery]">
										<div class="imgback">
											<img src="{{asset('images/veeracc13a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
											<div class="overlay">
												<div class="text">
													<span class="fas fa-search"></span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li data-thumb="{{asset('images/veeracc14.jpg')}}">
									<a class="image-zoom" href="{{asset('images/veeracc14b.jpg')}}" rel="prettyPhoto[gallery]">
										<div class="imgback">
											<img src="{{asset('images/veeracc14a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
											<div class="overlay">
												<div class="text">
													<span class="fas fa-search"></span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li data-thumb="{{asset('images/veeracc15.jpg')}}">
									<a class="image-zoom" href="{{asset('images/veeracc15b.jpg')}}" rel="prettyPhoto[gallery]">
										<div class="imgback">
											<img src="{{asset('images/veeracc15a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
											<div class="overlay">
												<div class="text">
													<span class="fas fa-search"></span>
												</div>
											</div>
										</div>
									</a>
								</li>
								<li data-thumb="{{asset('images/veeracc16.jpg')}}">
									<a class="image-zoom" href="{{asset('images/veeracc16b.jpg')}}" rel="prettyPhoto[gallery]">
										<div class="imgback">
											<img src="{{asset('images/veeracc16a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
											<div class="overlay">
												<div class="text">
													<span class="fas fa-search"></span>
												</div>
											</div>
										</div>
									</a>
								</li>
							</ul>
						</div>
					</div>
					<div class="col-lg-1"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc1')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc1d')!!}</p><br>
					<!--
					<p class="price1">NTD. 1,999</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v1" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/M46595793.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif
		<!-- @if($type=="2")
			<div class="row" id="veergif3">
				<div class="col-sm-6" align="center">
					<div class="col-lg-1"></div>
					<div class="col-lg-10 flexslider">
						/*
						<a class="image-zoom" href="{{asset('images/veergif3.gif')}}" rel="prettyPhoto[gallery]">
							<div class="imgback">
								<img src="{{asset('images/veergif3.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</div>
						</a> */
						
						<ul class="slides wow fadeInUp" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veeracc21.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc21b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc21a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc22.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc22b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc22a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc23.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc23b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc23a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc24.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc24b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc24a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc25.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc25b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc25a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-1"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc2')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc2d')!!}</p><br>
					/*
					<p class="price1">NTD. 1,699</p><br>*/
					<a href="{{route("ofami.veer",$locale)}}#v2" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/M46605471.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif -->
		@if($type=="2")
			<div class="work py-5 my-lg-5">
				<div class="container">
					<iframe width="100%" height="500" src="https://www.youtube.com/embed/KqkyJRC78Dk?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<div class="clearfix"></div><br>
				</div>
			</div>
			<div class="row" id="veergif3">
				<div class="col-sm-6" align="center">
					<div class="container">
						<!-- Nav pills Bootstrap 4 -->
						<ul class="nav nav-pills" role="tablist" style="float:right">
							<li onclick="ICECAMO1()"class="nav-item">
								<a class="nav-link active" data-toggle="pill" href="#home" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-IceCamoSwatch.png')}}" style="width:45px;height:45px; border-radius: 50%;" ></a>
							</li>
							<li onclick="Camo1()" class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#menu1" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-Camo.png')}}" style="width:45px;height:45px; border-radius: 50%;" ></a>
							</li>
							<li onclick="White1()" class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#menu2" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-Savanna-White-Icon.png')}}" style="width:45px;height:45px; border-radius: 50%;"></a>
							</li>
							<li  onclick="Black()" class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#menu3" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-black.png')}}" style="width:45px;height:45px; border-radius: 50%;"></a>
							</li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content "><br><br>
							<div id="home" class="container tab-pane active row"><br>
								<div class="col-md-12 imgback pad0">
									<a class="image-zoom" id="vhref4" href="{{asset('images/veericecamo1b.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon4" src="{{asset('images/veericecamo1b.jpg')}} " class="img-responsive zoom-img" />
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5>ICE CAMO</h5>
											</div>
										</div>
									</a>
								</div></br>
								<div class="row pad0">
									<div class="col-3 ">
										<a class="v17 ">
											<img src="{{asset('images/veericecamo1b.jpg')}}" >
										</a>
									</div>
									<div class="col-3 pad0">
										<a class="v17a ">
											<img src="{{asset('images/veericecamo2b.jpg')}}" >
										</a>
									</div>
									<div class="col-3 pad0">
										<a class="v17b ">
											<img src="{{asset('images/veericecamo3b.jpg')}}" >
										</a>
									</div>
								</div>
							</div>
							<div id="menu1" class="container tab-pane fade"><br>
								<div class="col-md-12 imgback">
									<a id="vherf7" href="{{asset('images/veercamo1.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon7" src="{{asset('images/veercamo1.jpg')}} " class="img-responsive zoom-img"/>
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5> CAMO</h5>
											</div>
										</div>
									</a>
								</div></br>
								<div class="col-md-12">
									<div class="row">
										<div class="col-3">
											<a class="v20">
												<img src="{{asset('images/veercamo1.jpg')}}" >
											</a>
										</div>
										<div class="col-3">
											<a class="v20a">
												<img src="{{asset('images/veercamo2.jpg')}}" >
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="menu2" class="container tab-pane fade"><br>
								<div class="col-md-12 imgback">
									<a id="vhref5" href="{{asset('images/veersavannawhite0b.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon5" src="{{asset('images/veersavannawhite0b.jpg')}} " class="img-responsive zoom-img" />
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5>WHITE</h5>
											</div>
										</div>
									</a>
								</div></br>
								<div class="col-md-12">
									<div class="row">
										<div class="col-3">
											<a class="v18">
												<img src="{{asset('images/veersavannawhite0b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v18a">
												<img src="{{asset('images/veersavannawhite1b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v18b">
												<img src="{{asset('images/veersavannawhite2b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v18c">
												<img src="{{asset('images/veersavannawhite3b.jpg')}}">
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="menu3" class="container tab-pane fade"><br>
								<div class="col-md-12 imgback">
									<a id="vhref6" href="{{asset('images/veeracc21b.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon6" src="{{asset('images/veeracc21b.jpg')}} " class="img-responsive zoom-img" >
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5>BLACK</h5>
											</div>
										</div>
									</a>
								</div></br>
								<div class="col-md-12">
									<div class="row">
										<div class="col-3">
											<a class="v19">
												<img src="{{asset('images/veeracc21b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v19a">
												<img src="{{asset('images/veeracc22b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v19b">
												<img src="{{asset('images/veeracc23b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v19c">
												<img src="{{asset('images/veeracc24b.jpg')}}" >
											</a>
										</div>
										<div class="col-3">
											<a class="v19d">
												<img src="{{asset('images/veeracc25b.jpg')}}" >
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc2')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc2d')!!}</p><br>
					<!--
					<p class="price1">NTD. 1,699</p><br> -->
					<a href="{{route("ofami.veer",$locale)}}#v2" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="https://www.pcstore.com.tw/ofami/M65862317.htm"  id="2" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買 ICE CAMO</a><br><br>
					<!--<a href="https://www.pcstore.com.tw/ofami/M65862631.htm" id="2" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買 CAMO</a>
					<a href="https://www.pcstore.com.tw/ofami/M65862971.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買 WHITE</a>
					<a href="https://www.pcstore.com.tw/ofami/M46605471.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買 BLACK</a><br><br> -->
					<br>
				</div>
			</div>	
		@endif	
		@if($type=="3")
			<div class="work py-5 my-lg-5">
				<div class="container">
                	<iframe width="100%" height="500" src="https://www.youtube.com/embed/uZ9StNhpGLI?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<div class="clearfix"></div><br>
				</div>
			</div>
			<div class="row" id="veergif7">
				<div class="col-md-6" align="center">
					<div class="col-lg-2"></div>
					<div class="col-lg-8 flexslider">
						<!--
						<a class="image-zoom" href="{{asset('images/veergif7.gif')}}" rel="prettyPhoto[gallery]">
							<div class="imgback">
								<img src="{{asset('images/veergif7.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</div>
						</a>-->
						<ul class="slides wow fadeInUp" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veeracc31.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc31b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc31a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc32.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc32b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc32a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc33.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc33b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc33a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc34.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc34b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc34a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc35.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc35b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc35a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc36.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc36b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc36a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<!--
							<li data-thumb="{{asset('images/veeracc37.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc37b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc37a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc38.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc38b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc38a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc39.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc39b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc39a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc310.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc310b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc310a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc311.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc311b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc311a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc312.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc312b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc312a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						-->
						</ul>
					</div>
					<div class="col-lg-2"></div>
					<p><br></p>
				</div>
				<div class="col-md-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc3')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc3d')!!}</p><br>
					<!--
					<p class="price1">NTD. 1,699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v3" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/S01JFPH.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif
		@if($type=="4")
			<div class="work py-5 my-lg-5">
				<div class="container">
                	<iframe width="100%" height="500" src="https://www.youtube.com/embed/cTY_LfBjjkI?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<div class="clearfix"></div><br>
				</div>
			</div>
			<div class="row" id="veergif5">
				<div class="col-sm-6" align="center">
					<div class="col-lg-2"></div>
					<div class="col-lg-8 flexslider">
						<!--
						<a class="image-zoom" href="{{asset('images/veergif5.gif')}}" rel="prettyPhoto[gallery]">
							<div class="imgback">
								<img src="{{asset('images/veergif5.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</div>
						</a>-->
						<ul class="slides wow fadeInUp" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veeracc41.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc41b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc41a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc42.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc42b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc42a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-2"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc4')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc4d')!!}</p><br>
					<!--
					<p class="price1">NTD. 2,699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v4" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/M46612095.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif
		@if($type=="5")
			<div class="row" id="veergif2">
				<div class="col-sm-6" align="center">
					<div class="col-lg-2"></div>
					<div class="col-lg-8 flexslider">
						<!--
						<a class="image-zoom" href="{{asset('images/veergif2.gif')}}" rel="prettyPhoto[gallery]">
							<div class="imgback">
								<img src="{{asset('images/veergif2.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</div>
						</a>-->
						<ul class="slides wow fadeInUp" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veeracc51.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc51b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc51a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc52.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc52b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc52a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc53.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc53b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc53a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc54.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc54b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc54a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc55.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc55b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc55a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-2"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc5')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc5d')!!}</p><br>
					<!--
					<p class="price1">NTD. 699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v5" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/M46611512.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif
		@if($type=="6")
			<div class="work py-5 my-lg-5">
				<div class="container">
                	<iframe width="100%" height="500" src="https://www.youtube.com/embed/IrehdLbc0mY?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="clearfix"></div><br>
			</div>
			<div class="row" id="veergif8">
				<div class="col-sm-6" align="center">
					<div class="col-lg-2"></div>
					<div class="col-lg-8 flexslider">
						<!--
						<a class="image-zoom" href="{{asset('images/veergif8.gif')}}" rel="prettyPhoto[gallery]">
							<div class="imgback">
								<img src="{{asset('images/veergif8.gif')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</div>
						</a>-->
						<ul class="slides wow fadeInUp" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veeracc61.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc61b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc61a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc62.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc62b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc62a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc63.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc63b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc63a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc64.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc64b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc64a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-2"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc7')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc7d')!!}</p><br>
					<!--
					<p class="price1">NTD. 2,699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v6" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/M46612354.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif
		@if($type=="7")
			<div class="row" id="veerac8">
				<div class="col-sm-6" align="center">
					<div class="col-lg-2"></div>
					<div class="col-lg-8">
						<a class="image-zoom wow fadeInUp" data-wow-duration="2s" href="{{asset('images/veerac8.jpg')}}" rel="prettyPhoto[gallery]">
							<div class="imgback">
								<img src="{{asset('images/veerac8.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer">
								<div class="overlay">
									<div class="text">
										<span class="fas fa-search"></span>
									</div>
								</div>
							</div>
						</a>
					</div>
					<div class="col-lg-2"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc8')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc8d')!!}</p><br>
					<!--
					<p class="price1">NTD. 699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v7" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="http://www.pcstore.com.tw/ofami/M46611236.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					<br>
				</div>
			</div>
		@endif
		@if($type=="8")
			<div class="work py-5 my-lg-5">
				<div class="container">
					<iframe width="100%" height="500" src="https://www.youtube.com/embed/rAjRwXwWRFA?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="clearfix"></div><br>
			</div>
			<div class="row" id="veergif8">
				<div class="col-sm-6" align="center">
					<div class="col-lg-2"></div>
					<div class="col-lg-8 flexslider">
						<ul class="slides wow fadeInUp" data-wow-duration="2s">
							<li data-thumb="{{asset('images/veeracc71.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc71b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc71a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc72.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc72b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc72a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc73.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc73b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc73a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc74.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc74b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc74a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
							<li data-thumb="{{asset('images/veeracc75.jpg')}}">
								<a class="image-zoom" href="{{asset('images/veeracc75b.jpg')}}" rel="prettyPhoto[gallery]">
									<div class="imgback">
										<img src="{{asset('images/veeracc75a.jpg')}}" class="img-responsive" alt="樂享學,ofami,拖車,veer,配件">
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
											</div>
										</div>
									</div>
								</a>
							</li>
						</ul>
					</div>
					<div class="col-lg-2"></div>
					<p><br></p>
				</div>
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc9')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc9d')!!}</p><br>
					<!--
					<p class="price1">NTD. 2,699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v6" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>　
					<a href="https://www.pcstore.com.tw/ofami/M65853489.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a> 
					<br>
				</div>
			</div>
		@endif
		 @if($type=="9")
		 	<div class="work py-5 my-lg-5">
				<div class="container">
					<iframe width="100%" height="500" src="https://www.youtube.com/embed/ATDwHTY9bcY?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
				</div>
				<div class="clearfix"></div><br>
			</div>
			<div class="row" id="veerac9">
				<div class="col-sm-6" align="center">
					<div class="container">
						<!-- Nav pills Bootstrap 4 -->
						<ul class="nav nav-pills" role="tablist" style="float:right">
							<li onclick="ICECAMO()" class="nav-item">
								<a class="nav-link active" data-toggle="pill" href="#home" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-IceCamoSwatch.png')}}" style="width:45px;height:45px; border-radius: 50%;" ></a>
							</li>
							<li onclick="camo()" class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#menu1" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-Camo.png')}}" style="width:45px;height:45px; border-radius: 50%;" ></a>
							</li>
							<li onclick="White()" class="nav-item">
								<a class="nav-link" data-toggle="pill" href="#menu2" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/color-Savanna-White-Icon.png')}}" style="width:45px;height:45px; border-radius: 50%;"></a>
							</li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content "><br><br>
							<div id="home" class="container tab-pane active row"><br>
								<div class="col-md-12 imgback pad0">
									<a class="image-zoom" id="vhref8" href="{{asset('images/veeracc91b.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon8" src="{{asset('images/veeracc91b.jpg')}} " class="img-responsive zoom-img" />
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5>ICE CAMO</h5>
											</div>
										</div>
									</a>
								</div><div class="row pad0">
									<div class="col-3 ">
										<a class="v21 ">
											<img src="{{asset('images/veeracc91b.jpg')}}" >
										</a>
									</div>
									<div class="col-3 pad0">
										<a class="v21b ">
											<img src="{{asset('images/veeracc92b.jpg')}}" >
										</a>
									</div>
								</div>
							</div>
							<div id="menu1" class="container tab-pane fade"><br>
								<div class="col-md-12 imgback">
									<a id="vherf2" href="{{asset('images/veeracc81b.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon2" src="{{asset('images/veeracc81b.jpg')}} " class="img-responsive zoom-img"/>
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5> CAMO</h5>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-3">
											<a class="v15">
												<img src="{{asset('images/veeracc81b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v15b">
												<img src="{{asset('images/veeracc82b.jpg')}}">
											</a>
										</div>
									</div>
								</div>
							</div>
							<div id="menu2" class="container tab-pane fade"><br>
								<div class="col-md-12 imgback">
									<a id="vhref1" href="{{asset('images/veeracc100b.jpg')}}" data-toggle="modal" data-target="#lightbox" rel="prettyPhoto[gallery]">
										<img id="vicon1" src="{{asset('images/veeracc100b.jpg')}} " class="img-responsive zoom-img"/>
										<div class="overlay">
											<div class="text">
												<span class="fas fa-search"></span>
												<h5>WHITE</h5>
											</div>
										</div>
									</a>
								</div>
								<div class="col-md-12">
									<div class="row">
										<div class="col-3">
											<a class="v14">
												<img src="{{asset('images/veeracc100b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v14a">
												<img src="{{asset('images/veeracc101b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v14b">
												<img src="{{asset('images/veeracc102b.jpg')}}">
											</a>
										</div>
										<div class="col-3">
											<a class="v14c">
												<img src="{{asset('images/veeracc104b.png')}}">
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> 		
				<div class="col-sm-6" align="left">
					<h2 class="my-2 wow fadeInUp" data-wow-duration="2s">{!!trans('veer.acc10')!!}</h2>
					<p class="wow fadeInUp">{!!trans('veer.acc10d')!!}</p><br>
					<!--
					<p class="price1">NTD. 699</p><br>-->
					<a href="{{route("ofami.veer",$locale)}}#v7" class="btn btn-outline-secondary wow fadeInUp" data-wow-duration="2s" role="button">Back</a>
					<a href="https://www.pcstore.com.tw/ofami/M65861205.htm" id="1"  class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買ICE CAMO</a>
					<!-- <a href="https://www.pcstore.com.tw/ofami/M65857596.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買CAMO</a>
					<a href="https://www.pcstore.com.tw/ofami/M65861765.htm" class="btn btn-outline-primary wow fadeInUp" data-wow-duration="2s" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買WHITE</a><br><br> -->
					<br>
				</div>
			</div>
		
		@endif 
	</div>
</section>
<!--//accessories-->

@endsection