<!-- footer -->	
<footer>
	<div class="container py-3 py-md-4">
		<div class="row">
			<div class="col-lg-4 col-md-12 wow fadeInRight">
				<p class="py-lg-4">© 2018 O-Fami Co., Ltd. All Rights Reserved | Design by <a href="http://www.W3Layouts.com" target="_blank">W3Layouts</a></p>
			</div>
			<div class="col-lg-4 col-md-12 logo text-center wow fadeInUp" style="padding:20px 0px;">
				<a href="{{route('ofami.index')}}">樂享學 O-Fami</a>
			</div>
			<div class="col-lg-4 col-md-12 wow fadeInLeft">
				<ul class="py-lg-4">
					<li class="mx-2"><a href="{{route("ofami.index",$locale)}}">{{trans('index.home')}}</a></li>
					<li class="mx-2"><a href="{{route("ofami.nikimotion",$locale)}}">Nikimotion</a></li>
					<li class="mx-2"><a href="{{route("ofami.veer",$locale)}}">Veer</a></li>
					<li class="mx-2"><a href="{{route("ofami.ebike",$locale)}}">{{trans('index.ebike')}}</a></li>
					<li class="mx-2"><a href="{{route("ofami.cafe",$locale)}}">{{trans('index.cafe')}}</a></li>
					<li class="mx-2"><a href="{{route("ofami.bikerental",[$locale,$nav="discount"])}}">{{trans('index.bikerental')}}</a></li>
					<li class="mx-2"><a href="{{route("ofami.service",$locale)}}">{{trans('index.contact')}}</a></li>
					<li class="mx-2"><a target="_blank" href="http://www.pcstore.com.tw/ofami/">{{trans('index.sc')}}</a></li>
				</ul>
			</div>
		</div>
	</div>
</footer>
<!-- footer -->
