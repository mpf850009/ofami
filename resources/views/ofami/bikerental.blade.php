@extends("ofami.main")
@section('content')
<style>
	 * {font-family: 'su1';}
	.grid1 a :hover{
		color:darkblue;
	}
	.active1{
		color:#555;
		transition: color .5s;
		padding:8px;
		border: none;
	outline: none;
	cursor: pointer;
	}	
	.active1:hover , .active2{
		color:white;
		background: #555;
	}
	.card-body{
		text-align:center;
		padding:10px 10px;
	}
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!-- trip -->
<section class="work py-5 my-lg-5">
	<div class="container">
		<div class="row">
			<div class="col-lg-2 hover1">
				<div id="b_nav" class="card bg-light mb-3">
					@include('ofami.bikerental_nav')
				</div><br>
			</div>
			<div class="col-lg-10" style="padding:0px 3em;">
				@include('ofami.bikerental_content')
			</div>
		</div>
	</div>
</section>
<!-- trip -->

@endsection