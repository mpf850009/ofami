
@extends("ofami.main")
@section('content')
@include("ofami.css.imgbig")

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->

<!-- apcm -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5"> 安平及奇美博物館 </h1>
        <h4 class="my-3"> 樂享學 - 安平 - 奇美博物館 (參考路線) </h4>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/anping1.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/anping1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,安平,奇美博物館,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/anping2.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/anping2.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,安平,奇美博物館,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3"></p>
	</div>
</section>
<!-- apcm -->
@endsection