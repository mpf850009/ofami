@extends("ofami.main")
@section('content')
@include("ofami.css.imgbig")

<style>
    span{
        font-size: 18px;
    }
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->


<!-- sdt -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5">台灣歷史博物館 - 四草大眾廟 </h1>
        <h4 class="my-3">樂享學 - 山海圳綠道 - 台江公園　<span>騎跡記錄 July, 2018 </span></h4>
        <video class="my-3" src="{{asset('videos/sicao1.mp4')}}" controls></video>
	</div>
</section>
<!-- sdt -->

@endsection