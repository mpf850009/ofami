@extends("ofami.main")
@section('content')
@include("ofami.css.imgbig")

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->

<!-- tsip -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5">台南科學園區 </h1>
        <h4 class="my-3">穩正 - 南科彩繪村一日遊(參考路線)</h4>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/path_tsip0.png')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/path_tsip0.png')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>    
        <p class="mb-3">從穩正公司出發，往北走小巷接至山海圳，右轉沿著環島1號線的牌子走，途中會經過國道一號及國道八號交匯處橋下，沿途風景美不勝收，羊腸小徑中看得出有用心規劃過的痕跡</p>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/path_tsip1.png')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/path_tsip1.png')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div> 
        <p class="mb-3">右轉接至山海圳</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path1.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path1.jpg')}}" class="img-responsive" alt="右轉山海圳">
                    <div class="overlay">點我放大</div>
                </a>
            </div> 
        </div>
        <div class="col-lg-6 col-sm-12"></div>
        <p class="mb-3">沿著山海圳往北直行</p>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom hover14" href="{{asset('images/path_tsip2.png')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/path_tsip2.png')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div> 
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path2.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path2.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div> 
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path3.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path3.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">沿路還可以看到有羊咩咩在路邊吃草</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path4.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path4.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        
        <p class="mb-3">跟著環島1號線的指示牌移動至河堤對岸，以免逆向騎乘</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path5.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path5.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path6.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path6.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        
        <p class="mb-3">進入山海圳國道一號和國道八號交匯處橋下自行車道</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path7.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path7.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path8.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path8.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path9.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path9.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path10.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path10.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path11.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path11.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path12.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path12.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">須按下按鈕才會顯示綠燈供單車通行</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path13.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path13.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path14.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path14.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">我們選擇順時針的路線，先至樹谷生活館附近休息拍照，再去旁邊的幾米公園走走，隨後左轉南科北路往北騎乘前往南科彩繪村</p>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/path_tsip3.png')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/path_tsip3.png')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div> 
        <p class="mb-3">提塘港自行車道末端會接至南134線道，左轉後隨著指標前往樹谷生活館休息</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path15.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path15.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path16.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path16.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">沿途兩旁路樹樹陰，讓騎乘自行車時的疲憊感降低許多！</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path17.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path17.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path18.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path18.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path19.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path19.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path20.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path20.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path21.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path21.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path22.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path22.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        
        <p class="mb-3">進入樹谷園區內，可以很容易的找到廁所、便利商店、餐廳等等休息的地方</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path23.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path23.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path24.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path24.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path25.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path25.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path26.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path26.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path27.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path27.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path28.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path28.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        
        <p class="mb-3">樹谷農場入園即提供一組飼料供大小朋友餵食園區內動物，非常適合親子同樂</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path29.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path29.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">後面的大片草原，可以全家大小一同野餐、追趕跑跳碰</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path30.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path30.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path31.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path31.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">前往幾米主題公園的路上，還有廁所的指示牌供單車車友們使用</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path32.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path32.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path33.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path33.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path34.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path34.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">若體力還許可的話，可以挑戰騎到善化的胡厝寮彩繪村</p>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/path_tsip4.png')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/path_tsip4.png')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">沿著南科北路一路向北騎到底後左轉</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path35.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path35.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path36.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path36.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">接著看到「百二甲」的牌子右轉進入後，沿著指標走就能找到有著彩繪的巷弄</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path37.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path37.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path38.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path38.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">出現「南科彩繪村 歡迎參觀」彩繪字樣的牆壁，映入眼簾的是各式不同畫風的圖案</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path39.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path39.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path40.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path40.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path41.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path41.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path42.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path42.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path43.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path43.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path44.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path44.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path45.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path45.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">騎累了還可以到附近吃個檸檬愛玉休息</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path46.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path46.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path47.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path47.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">因天色漸晚，吃完愛玉就準備回程，走原南科北路一路往南直到科技部南部科工局前廣場，有很多隻逗趣的狗兒可以拍照留念，若平常時間還許可的話，可以繼續南下至迎曦湖走走，伴隨著夕陽於湖畔旁騎著自行車，也別有一番情調</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path48.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path48.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path49.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path49.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path50.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path50.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/tsip_path51.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/tsip_path51.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,南科彩繪村,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
	</div>
</section>
<!-- tsip -->

@endsection