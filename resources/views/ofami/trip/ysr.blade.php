@extends("ofami.main")
@section('content')
@include("ofami.css.imgbig")

<style>
    span{
        font-size: 18px;
    }
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->


<!-- ysr -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5">台灣歷史博物館-鹽水溪山海圳一日遊 </h1>
        <h4 class="my-3">樂享學 / 穩正 - 台灣歷史博物館 - 鹽水溪山海圳一日遊(參考路線)</h4>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/path_ysr0.png')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/path_ysr0.png')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">從穩正出發，走後方小巷到底後右轉，過便橋後左轉直行接至山海圳綠道</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path1.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path2.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path2.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path3.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path3.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path4.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path4.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path5.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path5.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path6.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path6.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path7.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path7.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path8.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path8.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">左轉接至山海圳綠道後，直行一小段路程即可見到史博館補給站，亦可至館內參觀</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path9.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path9.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path10.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path10.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path11.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path11.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path12.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path12.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path13.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path13.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path14.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path14.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path15.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path15.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path16.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path16.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path17.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path17.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">重新規劃後的山海圳綠道自行車橋</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path19.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path19.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path20.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path20.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path21.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path21.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">從對面看起來的樣子</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path22.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path22.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">一路往西</p>
        <div class="row mb-3">
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path23.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path23.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path24.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path24.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path25.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path25.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path26.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path26.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path27.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path27.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-4 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path29.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path29.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">路途中皆有涼亭可供乘涼，休息之餘也可以看看山海圳綠道是如何規劃，地圖也顯示自行車綠道也可以一路延伸騎乘至黃金海岸風景區</p>
        <div class="row mb-3">
            <div class="col-lg-3 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path33.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path33.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path31.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path31.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path28.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path28.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-6">
                <a class="image-zoom" href="{{asset('images/ysr_path18.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path18.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">若要去黃金海岸，則可沿著省道17號公路往南騎乘</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path32.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path32.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">我們繼續直行前往山海圳綠道極西點</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path34.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path34.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path35.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path35.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path36.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path36.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path37.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path37.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path38.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path38.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path39.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path39.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3">抵達目的地 - 鹽水溪出海口</p>
        <div class="row mb-3">
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path40.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path40.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-6 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path42.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path42.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
            <div class="col-lg-3 col-sm-12">
                <a class="image-zoom" href="{{asset('images/ysr_path41.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/ysr_path41.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,鹽水溪出海口,歷史博物館,山海圳綠園道,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
	</div>
</section>
<!-- ysr -->

@endsection