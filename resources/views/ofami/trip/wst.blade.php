@extends("ofami.main")
@section('content')
@include("ofami.css.imgbig")

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->
<!-- wst -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5"> 烏山頭水庫 </h1>
        <h4 class="my-3"> 樂享學 - 烏山頭水庫 (參考路線) </h4>
        <div class="row mb-3">
            <div class="col-sm-12">
                <a class="image-zoom" href="{{asset('images/wst1.jpg')}}" rel="prettyPhoto[gallery]">
                    <img src="{{asset('images/wst1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,烏山頭水庫,ofami,ebike">
                    <div class="overlay">點我放大</div>
                </a>
            </div>
        </div>
        <p class="mb-3"></p>
	</div>
</section>
<!-- wst -->

@endsection