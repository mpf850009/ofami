<!-- Navigation -->
<header>
	<div class="top-nav">
		<div class="container">
			<nav class="navbar navbar-expand-lg navbar-light">
				<a class="navbar-brand text-uppercase" href="{{route('index',$locale)}}" style="font-size:35px;">
					<img src="{{asset('images/logo1.png')}}" style="width:160px;height:48px;"> O-Fami
				</a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse justify-content-center pr-md-4" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a @if($active=="index")class="nav-link active" @endif class="nav-link" href="{{route('index',$locale)}}">{{trans('index.home')}}<span class="sr-only">(current)</span></a>
						</li>
						<li class="dropdown nav-item">
							<a @if($active=="nikimotion"||$active=="veer"||$active=="veeracc"||$active=="nkacc"||$active=="nkaf"||$active=="nkafl"||$active=="blade")class="dropdown-toggle nav-link active" @endif class="dropdown-toggle nav-link" href="#" data-toggle="dropdown">{{trans('index.product')}}
								<b class="caret"></b>
							</a>
							<ul class="dropdown-menu agile_short_dropdown">
								<li>
									<a href="{{route("ofami.nikimotion",$locale)}}">Nikimotion</a>
								</li>
								<li>
									<a href="{{route("ofami.veer",$locale)}}">Veer</a>
								</li>
							</ul>
						</li>
						<li class="nav-item">
							<a @if($active=="ebike"||$active=="ebikeinfo")class="nav-link active" @endif class="nav-link" href="{{route("ofami.ebike",$locale)}}">{{trans('index.ebike')}}</a>
						</li>
						<li class="nav-item">
							<a @if($active=="cafe")class="nav-link active" @endif class="nav-link" href="{{route("ofami.cafe",$locale)}}">{{trans('index.cafe')}}</a>
						</li>
						<li class="nav-item">
							<a @if($active=="bikerental"||$active=="ysr"||$active=="tsip"||$active=="sdt"||$active=="xh"||$active=="wst"||$active=="hmtsip"||$active=="apcm")class="nav-link active" @endif class="nav-link" href="{{route("ofami.bikerental",[$locale,$nav="discount"])}}">{{trans('index.bikerental')}}</a>
						</li>
						<li class="nav-item">
							<a @if($active=="service")class="nav-link active" @endif class="nav-link" href="{{route("ofami.service",$locale)}}">{{trans('index.contact')}}</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="http://www.pcstore.com.tw/ofami/" target="_blank">{{trans('index.sc')}}</a>
						</li>
						@if($locale=="zh-TW")
						<li class="nav-item">
							<a class="nav-link" href="{{route("index",$locale="en")}}">English</a>
						</li>
						@elseif($locale=="en")
						<li class="nav-item">
							<a class="nav-link" href="{{route("index",$locale="zh-TW")}}">中文</a>
						</li>
						@endif
						<!-- search --->
						<!--
						<div class="search-bar-agileits">
							<div class="cd-main-header">
								<ul class="cd-header-buttons">
									<li><a class="cd-search-trigger" href="#cd-search"> <span></span></a></li>
								</ul>-->
								<!-- cd-header-buttons --><!--
							</div>
							<div id="cd-search" class="cd-search">
								<form action="#" method="post">
									<input name="Search" type="search" placeholder="Click enter after typing...">
								</form>
							</div>
						</div>-->
						<!-- search --->
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>
<!-- //Navigation -->