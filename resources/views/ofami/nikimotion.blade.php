@extends("ofami.main")
@section('content')
@include('ofami.script.nikimotionscript')
@include("ofami.css.imghover")
<style>
	.container h1{
		font-size:35px;
	}
	.container h2{
		font-size:28px;
	}
	.nksize .imgback .img-fluid{
		padding:3.5em 3.5em;
	}
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1"></div>
</section>
<!-- inner page banner -->

<!-- nikimotion -->
<!--
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Nikimotion </h1>
		<iframe class="wow jackInTheBox" data-wow-duration="2s" width="100%" height="500" src="https://www.youtube.com/embed/v4LNtvnJXZY?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</section>
-->
<!-- nikimotion -->

<!-- nk_product -->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Product 產品 </h1>
		<div class="row nksize">
			<div id="afl" class="col-sm-12 col-lg-4 wow fadeInDown" data-wow-duration="2s" align="center" style="padding:0px;">
				<a href="{{route('ofami.nkafl',$locale)}}">
					<div class="imgback" >
                        <img src="{{asset('images/nk12.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,autofold,lite">
						<h4>AUTOFOLD LITE </h4>
                        <div class="overlay" style="background-image: url('{{asset('images/afl9.jpg')}}'); background-repeat : no-repeat;background-size: contain;">
                        </div>
					</div>
				</a>
				<p class=""></p>
				<p></p>
			</div>
			<div id="af" class="col-sm-12 col-lg-4 wow fadeInRight" data-wow-duration="2s" align="center" style="padding:0px;">
				<a href="{{route('ofami.nkaf',$locale)}}">
					<div class="imgback" >
                        <img src="{{asset('images/nk13.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,autofold">
						<h4>AUTOFOLD</h4>
                        <div class="overlay" style="background-image: url('{{asset('images/afl6.jpg')}}'); background-repeat : no-repeat;background-size: contain;">
                        </div>
					</div>
				</a>
				<p class=""></p>
				<p></p>
			</div>
			<div id="blade" class="col-sm-12 col-lg-4 wow fadeInLeft" data-wow-duration="2s" align="center" style="padding:0px;">
				<a href="{{route('ofami.blade',$locale)}}">
					<div class="imgback">
                        <img src="{{asset('images/nk14.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,blade">
						<h4>BLADE</h4>
                        <div class="overlay" style="background-image: url('{{asset('images/afl7.jpg')}}'); background-repeat : no-repeat;background-size: contain;">
                        </div>
					</div>
				</a>
				<p class=""></p>
				<p></p>
			</div>
		</div>
	</div>
</section>
<!-- nk_product -->

<!--Accessories-->
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 id="acc" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Accessories 配件 </h1>
		<div class="row">
			<div class="col-sm-6 col-lg-3 wow fadeInRight" data-wow-duration="2s" id="nkac1">
				<a href="{{route('ofami.nkacc',$locale)}}#nkac1">
					<div class="imgback">
                        <img src="{{asset('images/nkac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.ch')}}</div>
                        </div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-lg-3 wow fadeInUp" data-wow-duration="2s" id="afac1">
				<a href="{{route('ofami.nkacc',$locale)}}#afac1">
					<div class="imgback">
                        <img src="{{asset('images/afac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.afa')}}</div>
                        </div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-lg-3 wow fadeInDown" data-wow-duration="2s" id="afac2">
				<a href="{{route('ofami.nkacc',$locale)}}#afac2">
					<div class="imgback">
                        <img src="{{asset('images/afac2.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.mn')}}</div>
                        </div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-lg-3 wow fadeInUp" data-wow-duration="2s" id="afac3">
				<a href="{{route('ofami.nkacc',$locale)}}#afac3">
					<div class="imgback">
                        <img src="{{asset('images/afac3.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.rc')}}</div>
                        </div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s" id="blac1">
				<a href="{{route('ofami.nkacc',$locale)}}#blac1">
					<div class="imgback">
                        <img src="{{asset('images/blac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.ba')}}</div>
                        </div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s" id="aflac1">
				<a href="{{route('ofami.nkacc',$locale)}}#aflac1">
					<div class="imgback">
                        <img src="{{asset('images/aflac1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.ch2')}}</div>
                        </div>
					</div>
				</a>
			</div>
			<div class="col-sm-6 col-lg-3 wow fadeInLeft" data-wow-duration="2s" id="blac5">
				<a href="{{route('ofami.nkacc',$locale)}}#blac5">
					<div class="imgback">
                        <img src="{{asset('images/blac5.png')}}" alt="樂享學,ofami,嬰兒車,nikimotion,accessory" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{{trans('nikimotion.ch4')}}</div>
                        </div>
					</div>
				</a>
			</div>
		</div>
	</div>
</section>
<!--//Accessories-->

@endsection