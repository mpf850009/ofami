@include("ofami.css.imgbig")
@if ($section=="discount")
    <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Discount 優惠活動 </h1>
    <div class="row">
        <div class="col-lg-1"></div>
        <div class="col-lg-10">
            <!--
            <a class="hvr-icon-grow wow fadeInUp" data-wow-duration="2s">
                <h4 class="my-2"><i class="fa fa-coffee hvr-icon"></i> [送] 租車即贈飲品 (冷泡茶或水二擇一) </h4>
            </a>
            <p></p><br>-->
            <a class="hvr-icon-spin wow fadeInUp" data-wow-duration="2s">
                <h4 class="my-2"><i class="fa fa-bicycle hvr-icon"></i> [贈] 租車體驗卷</h4>
                <p>※ 加入樂享學會員成為樂享人 <br> ※ 現金消費滿350元即可獲得租車體驗卷乙張</p><br>
                <img src="{{asset('images/dis3.jpg')}}" alt="樂享學 o-fami 租車 優惠 discount ">
            </a>
            <p></p>
        </div>
        <div class="col-lg-1"></div>
    </div>
    <p></p>
@endif
@if ($section=="price")
    <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Price 租車價格 </h1>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 wow fadeInUp" data-wow-duration="2s" style="text-align:center;">
            <img src="{{asset('images/ofami2.png')}}" alt="樂享學,租車旅遊,租車價格,電助腳踏車,ofami,ebike" srcset="">
        </div>
        <div class="col-lg-3"></div>
    </div>
    <p></p>
@endif
@if ($section=="rundown")
    <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> RunDown 租車流程 </h1>
    <div class="row">
        <div class="col-lg-3"></div>
        <div class="col-lg-6 wow fadeInUp" data-wow-duration="2s" style="text-align:center;">
            <img src="{{asset('images/ofami3.png')}}" alt="樂享學,租車旅遊,租車流程,電助腳踏車,ofami,ebike" srcset="">
        </div>
        <div class="col-lg-3"></div>
    </div>
    <p></p>
@endif
@if ($section=="guide")
    <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Pro-Guide 專業導覽 </h1>
    <a href="{{route('ofami.tsip',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 山海圳 - 樹谷生活科學館 - 幾米主題公園 - 善化吳厝寮彩繪村 - 科工局</h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，右轉沿著環島1號線的牌子走，途中會經過國道一號及國道八號交匯處橋下，沿途風景美不勝收，羊腸小徑中看得出有用心規劃過的痕跡 ...</p>
    <p></p>

    <a href="{{route('ofami.ysr',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 台灣歷史博物館 - 山海圳 - 鹽水溪出海口 </h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，左轉沿著環島1號線的牌子走，經過剛整理後的山海圳綠道自行車橋，沿途有不少涼亭可以停下來休息，吹著風跟著鹽水溪一路向西、眺望出海口 ...</p>
    <p></p>

    <a href="{{route('ofami.sdt',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 台灣歷史博物館 - 山海圳 - 四草大眾廟 </h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，左轉沿著環島1號線的牌子走，途中會經過 ...</p>
    <p></p>

    <a href="{{route('ofami.hmtsip',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 歷史博物館 - 山海圳 - 樹谷生活科學館 - 幾米主題公園 - 南科管理局台灣犬 - 迎曦湖 </h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，左轉沿著環島1號線的牌子走，途中會經過 ...</p>
    <p></p>

    <a href="{{route('ofami.xh',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 新化老街 </h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，右轉沿著環島1號線的牌子走，途中會經過國道一號及國道八號交匯處橋下 ...</p>
    <p></p>

    <a href="{{route('ofami.apcm',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 安平 - 奇美博物館 </h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，左轉沿著環島1號線的指標走，到達安平後可在圖中標示的景點走走看看，時間充裕的話，還可以挑戰至奇美博物館，達成一日雙博成就 ...</p>
    <p></p>

    <a href="{{route('ofami.wst',$locale)}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
        <h5 class="my-2"><i class="fa fa-arrow-right hvr-icon"></i>　樂享學 - 烏山頭水庫</h5>
    </a>
    <p class="wow fadeInUp" data-wow-duration="2s">從樂享學出發，往北走小巷接至山海圳，右轉沿著環島1號線的牌子走，途中會經過國道一號及國道八號交匯處橋下，騎至省道一號後一路往北走，延著嘉南大圳旁的堤防騎乘，即可看見烏山頭水庫 ...</p>
    <p></p>
@endif
@if ($section=="guide1")
    <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> 樂享學 </h1>
    <div class="row">
        <div class="col-lg-6">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">行程總覽</a>
                </li>
                <li class="nav-item">
                    <a class="fontcolor1 nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">1小時</a>
                </li>
                <li class="nav-item">
                    <a class="fontcolor1 nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">2小時</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" id="3hr-tab" data-toggle="tab" href="#3hr" role="tab" aria-controls="3hr" aria-selected="false">3小時 <i class="fa fa-arrow-up" aria-hidden="true"></i></a>
                </li>
            </ul>
            <div class="tab-content" id="myTabContent">
                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                    <br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="hm"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-success">
                        <h5 class="my-2">國立台灣歷史博物館 (4km/9km)</h5>
                    </a><br><br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="xh"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-success">
                        <h5 class="my-2">新化老街 (8km)</h5>
                    </a><br><br>
                    <a href="#" class="hvr-icon-wobble-horizontal btn btn-outline-primary">
                        <h5 class="my-2">山海圳手作綠道</h5>
                    </a><br><br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="ap"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-danger">
                        <h5 class="my-2">安平老街 (35km)</h5>
                    </a><br><br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="wst"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-danger">
                        <h5 class="my-2">烏山頭水庫 (38km)</h5>
                    </a><br><br>
                    <a href="#" class="hvr-icon-wobble-horizontal btn btn-outline-danger">
                        <h5 class="my-2">善化胡厝寮彩繪村</h5>
                    </a><br><br>
                </div>
                <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                    <br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="hm"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-success">
                        <h5 class="my-2">國立台灣歷史博物館 (4km/9km)</h5>
                    </a><br><br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="xh"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-success">
                        <h5 class="my-2">新化老街 (8km)</h5>
                    </a><br><br>
                </div>
                <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                    <br>
                    <a href="#" class="hvr-icon-wobble-horizontal btn btn-outline-primary">
                        <h5 class="my-2">山海圳手作綠道</h5>
                    </a><br><br>
                </div>
                <div class="tab-pane fade" id="3hr" role="tabpanel" aria-labelledby="3hr-tab">
                    <br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="ap"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-danger">
                        <h5 class="my-2">安平老街 (35km)</h5>
                    </a><br><br>
                    <a href="{{route("ofami.bikerental",[$locale,$nav="wst"])}}" class="hvr-icon-wobble-horizontal btn btn-outline-danger">
                        <h5 class="my-2">烏山頭水庫 (38km)</h5>
                    </a><br><br>
                    <a href="#" class="hvr-icon-wobble-horizontal btn btn-outline-danger">
                        <h5 class="my-2">善化胡厝寮彩繪村</h5>
                    </a><br><br>
                </div>
            </div>
        </div>
        <div class="col-lg-6">
            <a class="image-zoom pad1" href="{{asset('images/ofamimap1.jpg')}}" rel="prettyPhoto[gallery]">
                <img src="{{asset('images/ofamimap1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,專業導覽,電助腳踏車,ofami,ebike,國立台灣歷史博物館">
                <div class="overlay">點我放大</div>
            </a><br><br>
        </div>
    </div>
    <p></p>
@endif
@if ($section=="hm")
    <h1 id="size1" class="heading text-center text-uppercase mb-5"> 國立台灣歷史博物館 </h1>
    <div class="row">
        <div class="col-12 pad1" style="text-align:center;">
            <a class="image-zoom" href="{{asset('images/hm2.jpg')}}" rel="prettyPhoto[gallery]">
                <img src="{{asset('images/hm2.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,專業導覽,電助腳踏車,ofami,ebike,國立台灣歷史博物館">
                <div class="overlay">點我放大</div>
            </a>
        </div>
    </div>
    <p></p>
@endif
@if ($section=="xh")
    <h1 id="size1" class="heading text-center text-uppercase mb-5"> 台南新化老街 </h1>
    <div class="row">
        <div class="col-12 pad1" style="text-align:center;">
            <a class="image-zoom" href="{{asset('images/xinhua1.jpg')}}" rel="prettyPhoto[gallery]">
                <img src="{{asset('images/xinhua1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,專業導覽,電助腳踏車,ofami,ebike,台南,新化老街">
                <div class="overlay">點我放大</div>
            </a>
        </div>
    </div>
    <p></p>
@endif
@if ($section=="wst")
    <h1 id="size1" class="heading text-center text-uppercase mb-5"> 台南烏山頭水庫 </h1>
    <div class="row">
        <div class="col-12 pad1" style="text-align:center;">
            <a class="image-zoom" href="{{asset('images/wst1.jpg')}}" rel="prettyPhoto[gallery]">
                <img src="{{asset('images/wst1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,專業導覽,電助腳踏車,ofami,ebike,台南,烏山頭水庫">
                <div class="overlay">點我放大</div>
            </a>
        </div>
    </div>
    <p></p>
@endif
@if ($section=="ap")
    <h1 id="size1" class="heading text-center text-uppercase mb-5"> 安平老街 </h1>
    <div class="row">
        <div class="col-12 pad1" style="text-align:center;">
            <a class="image-zoom" href="{{asset('images/anping1.jpg')}}" rel="prettyPhoto[gallery]">
                <img src="{{asset('images/anping1.jpg')}}" class="img-responsive" alt="樂享學,租車旅遊,專業導覽,電助腳踏車,ofami,ebike,安平老街,安平">
                <div class="overlay">點我放大</div>
            </a>
        </div>
    </div>
    <p></p>
@endif
@if ($section=="ysrtsip")
    <h1 id="size1" class="heading text-center text-uppercase mb-5"> 南科山海圳 </h1>
    <div class="row">
        <div class="col-lg-2">
            <a href="{{route("ofami.bikerental",[$locale,$nav="hm"])}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
                <h5 class="my-2">國立台灣歷史博物館</h5>
            </a>
        </div>
        <div class="col-lg-8" style="text-align:center;">
            <img src="{{asset('images/hmtsip2.jpg')}}" alt="樂享學,租車旅遊,租車流程,電助腳踏車,ofami,ebike" srcset="">
        </div>
        <div class="col-lg-2">
            <a href="{{route("ofami.bikerental",[$locale,$nav="ysrtsip"])}}" class="hvr-icon-wobble-horizontal wow fadeInUp" data-wow-duration="2s">
                <h5 class="my-2">南科園區</h5>
            </a>
        </div>
    </div>
    <p></p>
@endif