<style>
.imgback {
  position: relative;
}

.image {
  display: block;
  width: 100%;
  height: auto;
}

.overlay {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  background-color: rgba(0,0,0,0.5);
  overflow: hidden;
  width: 100%;
  height: 0;
  transition: .5s ease;
}

.imgback:hover .overlay {
  height: 100%;
}

.text {
  color: #f5f6f7;
  font-size: 24px;
  position: absolute;
  top: 50%;
  left: 50%;
  -webkit-transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  transform: translate(-50%, -50%);
  text-align: center;
}
.imgback h4{
	position: absolute;
    bottom: 0;
    color: #fff;
    background-color: rgba(0, 0, 0, 0.5);
    width:100%;
    font-size: 2em;
    padding: 0.5em;
    text-align: center;

	}
</style>