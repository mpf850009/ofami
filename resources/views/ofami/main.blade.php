<!DOCTYPE html>
<html>
<head>
<title>{{$title}}</title>
	<!-- Meta tag Keywords -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="utf-8">
	<meta name="keywords" content="樂享學,早午餐,咖啡輕食,嬰兒車,電動腳踏車,親子餐廳,ofami,O-Fami,cafe,nikimotion,veer,ebike"/>
	<meta name="msvalidate.01" content="E27A2B5E084140465F375DE8DB588918" />
	<!-- Meta tag Keywords -->

	<!--Hover-->
	<link rel="stylesheet" href="{{asset('css/hover.css')}}" type="text/css" media="all" />
	
	<!--Animate-->
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.0/animate.min.css">

	
	<link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
	
	<!-- css files -->
	<link rel="stylesheet" href="{{asset('css/bootstrap.css')}}"> <!-- Bootstrap-Core-CSS -->
	<link rel="stylesheet" href="{{asset('css/style.css')}}" type="text/css" media="all" /> <!-- Style-CSS --> 
	<link rel="stylesheet" href="{{asset('css/fontawesome-all.css')}}"> <!-- Font-Awesome-Icons-CSS -->
	<!-- css files -->
	<link href="{{asset('css/prettyPhoto.css')}}" rel="stylesheet" type="text/css" />

	<!-- web-fonts -->
	<link href="//fonts.googleapis.com/css?family=Alegreya+Sans:100,100i,300,300i,400,400i,500,500i,700,700i,800,800i,900,900i&amp;subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,vietnamese" rel="stylesheet">
	
    <!--網站圖標-->
    <link rel="shortcut icon" href="{{asset('favicon.ico')}}" type="image/x-icon" />
    
    <!--官網縮圖-->
	<meta property="og:image" content="{{asset('images/ofamilogo.jpg')}}" />

	<style>
		@font-face {
			font-family: 'su';
			src: url('../webfonts/GenJyuuGothic-Regular.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
		} 
		@font-face {
			font-family: 'su1';
			src: url('../../webfonts/GenJyuuGothic-Regular.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
		}
		@font-face {
			font-family: 'price';
			src: url('../webfonts/century-gothic.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
		}
		
		@font-face {
			font-family: 'price1';
			src: url('../../webfonts/century-gothic.ttf') format('truetype'); /* Chrome 4+, Firefox 3.5, Opera 10+, Safari 3—5 */
		}
		* {font-family: 'su';}
		#size1{
			font-size: 35px;
		}
	</style>
	
</head>

<body>

@include("ofami.nav")
@yield("content")
@include("ofami.footer")
@include("ofami.facebook")

<!-- js-scripts -->	

	
	<!-- js -->
	<script type="text/javascript" src="{{asset('js/jquery-2.1.4.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/bootstrap.js')}}"></script> <!-- Necessary-JavaScript-File-For-Bootstrap --> 
	<!-- //js -->	

	<script src="https://code.jquery.com/jquery-1.8.0.min.js"></script>
	<script src="{{asset('js/flexslider/jquery.flexslider.js')}}"></script>
	<script type="text/javascript" charset="utf-8">
	$(window).load(function() {
		$('.flexslider').flexslider({
			animation: "slide",
			controlNav: "thumbnails"
		});
	});
	</script>
	
	<!-- Banner Responsive slider -->
	<script src="{{asset('js/responsiveslides.min.js')}}"></script>
	<script>
		// You can also use "$(window).load(function() {"
		$(function () {
			// Slideshow 3
			$("#slider3").responsiveSlides({
				auto: true,
				pager: false,
				nav: true,
				speed: 500,
				namespace: "callbacks",
				before: function () {
					$('.events').append("<li>before event fired.</li>");
				},
				after: function () {
					$('.events').append("<li>after event fired.</li>");
				}
			});

		});
	</script>
	<!-- // Banner Responsive slider -->
	
	<!-- stats -->
	<script src="{{asset('js/jquery.waypoints.min.js')}}"></script>
	<script src="{{asset('js/jquery.countup.js')}}"></script>
	<script>
		$('.counter').countUp();
	</script>
	<!-- //stats -->
	
	<!-- search-bar -->
	<script src="{{asset('js/main.js')}}"></script>
	<!-- //search-bar -->
	
	<!-- start-smoth-scrolling -->
	<script src="{{asset('js/SmoothScroll.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/move-top.js')}}"></script>
	<script type="text/javascript" src="{{asset('js/easing.js')}}"></script>
	<script type="text/javascript">
		// jQuery(document).ready(function($) {
		// 	$(".scroll").click(function(event){		
		// 		event.preventDefault();
		// 		$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
		// 	});
		// });
	</script>
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/	
			// $().UItoTop({ easingType: 'easeOutQuart' });						
		});
	</script>
	<!-- //here ends scrolling icon -->
	<!-- start-smoth-scrolling -->

	<!-- jQuery-Photo-filter-lightbox-Gallery-plugin -->
	<script type="text/javascript" src="{{asset('js/jquery-1.7.2.js')}}"></script>
	<script src="{{asset('js/jquery.quicksand.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/script.js')}}" type="text/javascript"></script>
	<script src="{{asset('js/jquery.prettyPhoto.js')}}" type="text/javascript"></script>
	<!-- //jQuery-Photo-filter-lightbox-Gallery-plugin -->

<!-- //js-scripts -->


<!--Animate js-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script>
    var wow = new WOW(
  {
    boxClass:     'wow',      // 要套用WOW.js縮需要的動畫class(預設是wow)
    animateClass: 'animated', // 要"動起來"的動畫(預設是animated, 因此如果你有其他動畫library要使用也可以在這裡調整)
		offset:       50,          // 距離顯示多遠開始顯示動畫 (預設是0, 因此捲動到顯示時才出現動畫)
    mobile:       true,       // 手機上是否要套用動畫 (預設是true)
    live:         true,       // 非同步產生的內容是否也要套用 (預設是true, 非常適合搭配SPA)
    callback:     function(box) {
      // 當每個要開始時, 呼叫這裡面的內容, 參數是要開始進行動畫特效的element DOM
    },
    scrollContainer: null // 可以設定成只套用在某個container中捲動才呈現, 不設定就是整個視窗
  }
);
wow.init();
</script>


</body>
</html>