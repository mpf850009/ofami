@extends("ofami.main")
@section('content')
@include('ofami.script.ebikescript')

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<style>
    * {font-family: 'su1';}
    .row ol li{
        font-size:23px;
    }
    .row ul li{
        font-size:23px;
	}
	.price{
		text-align: right;
		font-family:price1;
		font-size: 35px;
	}
	.price1{
		float: left;
		font-family:price1;
		font-size: 35px;
	}

    .pad0{
        padding: 0px 0px;
    }
    .pad15{
        padding: 15px 15px;
    }
	
	#lightbox .modal-content {
		display: inline-block;
		text-align: center;   
	}

	#lightbox .close {
		opacity: 1;
		color: rgb(255, 255, 255);
		background-color: rgb(25, 25, 25);
		padding: 5px 8px;
		border-radius: 30px;
		border: 2px solid rgb(255, 255, 255);
		position: absolute;
		top: -15px;
		right: -55px;
		z-index:1032;
	}
	.myImg{
		opacity: 0.6;
		-webkit-transform: scale(1);
		transform: scale(1);
		-webkit-transition: .5s ease-in-out;
		-moz-transition: .5s ease-in-out;
		-o-transition: .5s ease-in-out;
		transition: .5s ease-in-out;
	}
	.myImg:hover {
		opacity: 1;
		-webkit-transform: scale(1.2);
		transform: scale(1.2);
	}
	figure.snip1104 {
		padding: 0 0;
		margin: 0 0;
		font-family: 'Raleway', Arial, sans-serif;
		position: relative;
		overflow: hidden;
		width: 100%;
		background: #000000;
		color: #ffffff;
		text-align: center;
		box-shadow: 0 0 5px rgba(0, 0, 0, 0.15);
        /* border-radius:10px; */
	}

	figure.snip1104 * {
		-webkit-box-sizing: border-box;
		box-sizing: border-box;
		-webkit-transition: all 0.4s ease-in-out;
		transition: all 0.4s ease-in-out;
	}

	figure.snip1104 img {
		max-width: 100%;
		position: relative;
		opacity: 0.4;
	}

	figure.snip1104 figcaption {
		padding: 0 0;
		position: absolute;
		top: 0;
		left: 0;
		bottom: 0;
		right: 0;
	}

	figure.snip1104 h2 {
		position: absolute;
		left: 40px;
		right: 40px;
		display: inline-block;
		background: #000000;
		-webkit-transform: skew(-10deg) rotate(-10deg) translate(0, -50%);
		transform: skew(-10deg) rotate(-10deg) translate(0, -50%);
		padding: 5px 5px;
		margin: 0 0;
		top: 50%;
		text-transform: uppercase;
		font-weight: 400;
        font-size: 21px;
        padding: 10px 5px;
	}

	figure.snip1104 h2 span {
		font-weight: 800;
	}

	figure.snip1104:before {
		height: 93%;
		width: 100%;
		top: 0;
		left: 0;
		content: '';
		background: #ffffff;
		position: absolute;
		-webkit-transition: all 0.3s ease-in-out;
		transition: all 0.3s ease-in-out;
		-webkit-transform: rotate(110deg) translateY(-50%);
		transform: rotate(110deg) translateY(-50%);
	}

	figure.snip1104 a {
		left: 0;
		right: 0;
		top: 0;
		bottom: 0;
		position: absolute;
		z-index: 1;
	}
	figure.snip1104:hover img,
	figure.snip1104.hover img {
		opacity: 1;
		-webkit-transform: scale(1.1);
		transform: scale(1.1);
	}

	figure.snip1104:hover h2,
	figure.snip1104.hover h2 {
		-webkit-transform: skew(-10deg) rotate(-10deg) translate(-150%, -50%);
		transform: skew(-10deg) rotate(-10deg) translate(-150%, -50%);
	}

	figure.snip1104:hover:before,
	figure.snip1104.hover:before {
		-webkit-transform: rotate(110deg) translateY(-150%);
		transform: rotate(110deg) translateY(-150%);
	}

    .imageborder{
        border: 2px solid grey;
        border-radius: 10px;
        padding: 10px 10px;
    }
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--EBike-->
<section class="work py-5 my-lg-5">
	<div class="container">
    <h1 id="size1" class="heading text-center text-lowercase mb-5"> 
		@if($type=="eb20") {{trans('ebike.bike5')}}
		@elseif($type=="eb26") {{trans('ebike.bike6')}}
		@elseif($type=="ebflat") {{trans('ebike.bike2')}}
		@elseif($type=="ebcurve") {{trans('ebike.bike3')}}
		@elseif($type=="ebfat") {{trans('ebike.bike1')}}
		@elseif($type=="ebknk") {{trans('ebike.bike4')}}
		@endif
	</h1>
    @if($type=="eb20")
		<div class="row">
			<div class="col-md-6" align="center">
				<div class="col-md-12">
					<div class="col-md-12">
						<img src="{{asset('images/ebike1.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,20吋,ebike" class="img-fluid">
					</div>
				</div>
				<p><br></p>
			</div>
			<div class="col-md-6" align="left">
				<br>
				{!!trans('ebike.bike5d')!!}
				<br>
				<p class="price1">NTD. 38,800</p><br>
			</div>
        </div>
    @endif
    @if($type=="eb26")
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<div class="container">
					<!-- Nav pills Bootstrap 4 -->
					<ul class="nav nav-pills" role="tablist" style="float:right">
						<li class="nav-item">
							<a class="nav-link active" data-toggle="pill" href="#home" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/ikin ez i-bike/grey.png')}}" style="width:45px;height:45px; border-radius: 50%;" title="星辰銀"></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="pill" href="#menu1" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/ikin ez i-bike/orange.png')}}" style="width:45px;height:45px; border-radius: 50%;" title="活力橘"></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" data-toggle="pill" href="#menu2" style="background:none;width:60px;height:60px;padding:10px 5px;"><img src="{{asset('images/ikin ez i-bike/darkblue.png')}}" style="width:45px;height:45px; border-radius: 50%;" title="閃靛藍"></a>
						</li>
					</ul>
				
					<!-- Tab panes -->
					<div class="tab-content"><br><br>
						<div id="home" class="container tab-pane active row"><br>
                            <div class="col-md-12 pad0">
								<a id="changebikehref2" href="{{asset('images/ikin ez i-bike/ez11.jpg')}}" data-toggle="modal" data-target="#lightbox">
									<img id="changebikesrc2" src="{{asset('images/ikin ez i-bike/ez11.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike"/>
								</a>
							</div>
							<div class="row pad0">
								<div class="col-3 pad0">
									<a class="btnchangebike11">
										<img src="{{asset('images/ikin ez i-bike/ez11.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
									</a>
								</div>
								<div class="col-3 pad0">
									<a class="btnchangebike12">
										<img src="{{asset('images/ikin ez i-bike/ez12.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
									</a>
								</div>
								<div class="col-3 pad0">
									<a class="btnchangebike13">
										<img src="{{asset('images/ikin ez i-bike/ez13.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
									</a>
								</div>
								<div class="col-3 pad0">
									<a class="btnchangebike14">
										<img src="{{asset('images/ikin ez i-bike/ez14.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
									</a>
								</div>
							</div>
						</div>
						<div id="menu1" class="container tab-pane fade"><br>
							<div class="col-md-12">
								<a id="changebikehref1" href="{{asset('images/ikin ez i-bike/ez01.jpg')}}" data-toggle="modal" data-target="#lightbox">
									<img id="changebikesrc1" src="{{asset('images/ikin ez i-bike/ez01.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike"/>
								</a>
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-3">
										<a class="btnchangebike01">
											<img src="{{asset('images/ikin ez i-bike/ez01.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
										</a>
									</div>
									<div class="col-3">
										<a class="btnchangebike02">
											<img src="{{asset('images/ikin ez i-bike/ez02.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
										</a>
									</div>
								</div>
							</div>
						</div>
						<div id="menu2" class="container tab-pane fade"><br>
							<div class="col-md-12">
								<a id="changebikehref3" href="{{asset('images/ikin ez i-bike/ez21.jpg')}}" data-toggle="modal" data-target="#lightbox">
									<img id="changebikesrc3" src="{{asset('images/ikin ez i-bike/ez21.jpg')}} " class="img-responsive zoom-img" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike"/>
								</a>
							</div>
							<div class="col-md-12">
								<div class="row">
									<div class="col-3">
										<a class="btnchangebike21">
											<img src="{{asset('images/ikin ez i-bike/ez21.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
										</a>
									</div>
									<div class="col-3">
										<a class="btnchangebike22">
											<img src="{{asset('images/ikin ez i-bike/ez22.jpg')}}" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike">
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="container">
					<p class="price"><br>NTD. 39,999&nbsp;&nbsp;</p><br>
					<h3>ez i-bike產品描述</h3><br>
					<p>● 6061 鋁合金車架<br>● MPF Drive 中置馬達系統<br>● 馬達配置於車架中間，使騎乘重心平穩、輕鬆靈活。<br>● 利用扭力、速度感應及踩踏頻率，提供不同的動力支援;讓你在平路騎乘順暢、爬坡輕鬆省力。<br>
						● 車架可摺疊設計、LED 前燈、前置物籃、後貨架<br>● 輪徑:20”451 輪組<br>● 顏色:星辰銀、活力橘、閃靛藍<br>● 高效鋰電池 400Wh(36V-11.6Ah)<br>● 充電時間:4-6 小時(首次使用建議充滿 6 小時)<br>● 續航力:35-80KM<br>
						<span style="font-size:14px">*說明:測量狀態為載重 65kg，使用容量 36V/11.6Ah 的全新電池，在車速 18km/h、 風速 2-3 級、常溫 25 度的情況下，且路面平坦胎壓正常。</span>
					</p><br><br>
					<h3>MPF 中置馬達系統特點</h3><br>
					<p>● 最大功率: 385 W<br>● 額定功率: 250 W<br>● 最大扭力: 60 Nm<br>● 時速最高:25 km/h<br>● 電動助力:10 段</p><br>
					<h3>ez i-bike 車款簡介</h3><br>
					<div class="row">
						<div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
							<a href="{{asset('images/ikin ez i-bike/ez32.jpg')}}" data-toggle="modal" data-target="#lightbox">
								<figure class="snip1104">
									<img src="{{asset('images/ikin ez i-bike/ez32.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 中置馬達" style="padding:0 0;" />
									<figcaption style="padding:0 0;">
										<h2>中置馬達<span></span></h2>
									</figcaption>
								</figure>
							</a>
						</div>
						<div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
							<a href="{{asset('images/ikin ez i-bike/I4US-1.jpg')}}" data-toggle="modal" data-target="#lightbox">
								<figure class="snip1104">
									<img src="{{asset('images/ikin ez i-bike/I4US-1.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 儀表控制系統" style="padding:0 0;" />
									<figcaption style="padding:0 0;">
										<h2>儀表控制系統<span></span></h2>
									</figcaption>
								</figure>
							</a>	
						</div>
						<div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
							<a href="{{asset('images/ikin ez i-bike/ez33.jpg')}}" data-toggle="modal" data-target="#lightbox">
								<figure class="snip1104">
									<img src="{{asset('images/ikin ez i-bike/ez33.jpg')}}" title="大容量鋰電池" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 大容量鋰電池 36V,11.6Ah" style="padding:0 0;" />
									<figcaption style="padding:0 0;">
										<h2>大容量鋰電池<span>36V,11.6Ah</span></h2>
									</figcaption>
								</figure>
							</a>
						</div>
						<div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
							<a href="{{asset('images/ikin ez i-bike/ez31.jpg')}}" data-toggle="modal" data-target="#lightbox">
								<figure class="snip1104">
									<img src="{{asset('images/ikin ez i-bike/ez31.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike SHIMANO 內變3速" style="padding:0 0;"/>
									<figcaption style="padding:0 0;">
										<h2>SHIMANO<span>內變3速</span></h2>
									</figcaption>
								</figure>
							</a>
						</div>
						<div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
							<a href="{{asset('images/ikin ez i-bike/ez34.jpg')}}" data-toggle="modal" data-target="#lightbox">
								<figure class="snip1104">
									<img src="{{asset('images/ikin ez i-bike/ez34.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 前置物籃" />
									<figcaption>
										<h2>前置物籃<span></span></h2>
									</figcaption>
								</figure>
							</a>
						</div>
						<div class="col-lg-4 col-sm-6 pad0 wow fadeInUp" data-wow-duration="2s">
							<a href="{{asset('images/ikin ez i-bike/ez35.jpg')}}" data-toggle="modal" data-target="#lightbox">
								<figure class="snip1104">
									<img src="{{asset('images/ikin ez i-bike/ez35.jpg')}}" title="電動輔助自行車" alt="ebike 創星 innovative 樂享學 電動輔助自行車 ikin ez i-bike 雙眼型LED前燈" />
									<figcaption>
										<h2>雙眼型LED前燈<span></span></h2>
									</figcaption>
								</figure>
							</a>
						</div>
					</div>
				</div>

				<div class="container"><br>
                    <h3>產品規格</h3><br>
                    <table class="table table-striped">
						<thead>
							<tr>
								<th>配件</th>
								<th>規格</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>車架尺寸</td>
								<td>1555*590*1128 MM</td>
							</tr>
							<tr>
								<td>重量</td>
								<td>23.5 KG(不包含電池)</td>
							</tr>
							<tr>
								<td>車架</td>
								<td>鋁合金#6061</td>
							</tr>
							<tr>
								<td>前叉</td>
								<td>鋁合金 Grind20”雙避震前叉</td>
							</tr>
							<tr>
								<td>煞車組</td>
								<td>STARRY 鋁合金 V 煞/RA-955AG</td>
							</tr>
							<tr>
								<td>齒盤</td>
								<td>舜興 BCD104-42T-6061</td>
							</tr>
							<tr>
								<td>鍊條</td>
								<td>DAYA-410HGST300</td>
							</tr>
							<tr>
								<td>飛輪</td>
								<td>SHIMANO-內變 3 速</td>
							</tr>
							<tr>
								<td>車手把</td>
								<td>鋁合金手把</td>
							</tr>
							<tr>
								<td>輪胎</td>
								<td>KENDA-防穿刺胎 20*1-3/8</td>
							</tr>
							<tr>
								<td>輪圈</td>
								<td>享勵-20*1-3/8 雙層鋁圈</td>
							</tr>
							<tr>
								<td>鋼絲</td>
								<td>白鐵 CP NIPPLES</td>
							</tr>
							<tr>
								<td>腳踏</td>
								<td>折疊帶反光片</td>
							</tr>
							<tr>
								<td>停車架</td>
								<td>雙腳中柱</td>
							</tr>
						</tbody>
                    </table>
				</div>
			</div>
			<div class="col-md-1"></div>

			{{-- <div class="col-md-6" align="center">
				<div class="col-md-12">
					<img src="{{asset('images/ebike2.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,26吋,ebike" class="img-fluid">
				</div>
				<p><br></p>
			</div>
			<div class="col-md-6" align="left">
				<br>
				{!!trans('ebike.bike6d')!!}
				<br>
				<p class="price1">NTD. 39,999</p>
				<br>
			</div> --}}
        </div>
    @endif
    @if($type=="ebflat")
		<div class="row">
			<div class="col-md-6" align="center">
				<div class="col-md-12" align="left">
					<img src="{{asset('images/ebike3.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,平把,ebike" class="img-responsive img-fluid">
					<p class="price">
						NTD. 60,800<br>
						<a href="http://www.pcstore.com.tw/ofami/M51436813.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					</p>
					<br>
					{!!trans('ebike.bikerule')!!}
				</div>
				<p><br></p>
			</div>
			<div class="col-md-6" align="left">
				<table class="table table-striped ">
                    {!!trans('ebike.bike2d')!!}
                </table>
				<br>
			</div>
        </div>
    @endif

    @if($type=="ebcurve")    
		<div class="row">
			<div class="col-md-6" align="center">
				<div class="col-md-12" align="left">
					<img src="{{asset('images/ebike4.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,彎把,ebike" class="img-responsive img-fluid">
					<p class="price">
						NTD. 69,800<br>
						<a href="http://www.pcstore.com.tw/ofami/M51438026.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					</p><br>
					{!!trans('ebike.bikerule')!!}
				</div>
				<p><br></p>
			</div>
			<div class="col-md-6" align="left">
				<table class="table table-striped ">
                    {!!trans('ebike.bike3d')!!}
                </table>
				<br>
			</div>
        </div>
    @endif
    @if($type=="ebfat")
		<div class="row">
			<div class="col-md-6" align="center">
				<div class="col-md-12" align="left">
					<img src="{{asset('images/ebike5.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,胖胎,ebike" class="img-responsive img-fluid">
					<p class="price">
						NTD. 79,800<br>
						<a href="http://www.pcstore.com.tw/ofami/M51435143.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
					</p><br>
					{!!trans('ebike.bikerule')!!}
				</div>
				<p><br></p>
			</div>
			<div class="col-md-6" align="left">
				<table class="table table-striped ">
                    {!!trans('ebike.bike1d')!!}
                </table>
				<br>
			</div>
        </div>
    @endif
    @if($type=="ebknk")
		<div class="row">
			<div class="col-md-6" align="center">
				<div class="col-md-12" align="left">
                    <img src="{{asset('images/ebike6.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,K&K,ebike" class="img-responsive img-fluid">
                    {!!trans('ebike.bikerule')!!}
				</div>
				<p><br></p>
			</div>
			<div class="col-md-6" align="left">
				<table class="table table-striped ">
                    {!!trans('ebike.bike4d')!!}
                </table>
				<br>
			</div>
        </div>
    @endif
    
    <a href="{{route('ofami.ebike',$locale)}}" class="btn btn-outline-secondary" role="button"> 　Back　 </a>
	</div>
</section>
<!--//ebike-->


<div id="lightbox" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<button type="button" class="close hidden" data-dismiss="modal" aria-hidden="true">×</button>
		<div class="modal-content">
			<div class="modal-body">
				<img src="" alt="" />
			</div>
		</div>
	</div>
</div>
<script>
$(document).ready(function() {
    var $lightbox = $('#lightbox');
    
    $('[data-target="#lightbox"]').on('click', function(event) {
        var $img = $(this).find('img'), 
            src = $img.attr('src'),
            alt = $img.attr('alt'),
            css = {
                'maxWidth': $(window).width(),
                'maxHeight': $(window).height()
            };
    
        $lightbox.find('.close').addClass('hidden');
        $lightbox.find('img').attr('src', src);
        $lightbox.find('img').attr('alt', alt);
        $lightbox.find('img').css(css);
    });
    
    $lightbox.on('shown.bs.modal', function (e) {
        var $img = $lightbox.find('img');
            
        $lightbox.find('.modal-dialog').css({'width': $img.width()});
        $lightbox.find('.close').removeClass('hidden');
    });
});
</script>
@endsection