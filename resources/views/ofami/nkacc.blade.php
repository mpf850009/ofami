@extends("ofami.main")
@section('content')
@include('ofami.script.nikimotionscript')
<style>
	.container h1{
		font-size:35px;
	}
	.row h2{
		color:black;
		font-size:28px;
		line-height: 40px;
	}
	.row ul{
		list-style-image:url('../images/nklogo3.png');
		list-style-position: inside;
	}
	.price{
		font-family:price;
		font-size: 25px;
	}
</style>
<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!--Accessories-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 class="heading text-center text-uppercase mb-5"> {{trans('nikimotion.afacc')}} </h1>
		<div class="row">
			<div id="nkac1" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="col-sm-12">
						<a class="image-zoom" href="{{asset('images/nkac1.jpg')}}" rel="prettyPhoto[gallery]">
							<img src="{{asset('images/nkac1.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<div align="right">
				</div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.ch')}}</h2>
				<p>{!!trans('nikimotion.ch1')!!}</p><br>
				<!--
				<p class="price">NTD. 699　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#nkac1" class="btn btn-outline-secondary" role="button">Back</a>　
				<a href="http://www.pcstore.com.tw/ofami/M46662464.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<br>
			</div>
		</div>
		<div class="row">
			<div id="aflac1" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="col-sm-12">
						<a class="image-zoom" href="{{asset('images/aflac1.jpg')}}" rel="prettyPhoto[gallery]">
							<img src="{{asset('images/aflac1.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<div align="right">
				</div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.ch2')}}</h2>
				<p>{!!trans('nikimotion.ch3')!!}</p><br>
				<!--
				<p class="price">NTD. 899　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#aflac1" class="btn btn-outline-secondary" role="button">Back</a>　
				<a href="https://www.pcstore.com.tw/ofami/M65852806.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a> 
				<br>
			</div>
		</div>
		<div class="row">
			<div id="blac5" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="col-sm-12">
						<a class="image-zoom" href="{{asset('images/blac5.png')}}" rel="prettyPhoto[gallery]">
							<img src="{{asset('images/blac5.png')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<div align="right">
				</div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.ch4')}}</h2>
				<p>{!!trans('nikimotion.ch5')!!}</p><br>
				<!--
				<p class="price">NTD. 1050　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#blac5" class="btn btn-outline-secondary" role="button">Back</a>　
				<a href="https://www.pcstore.com.tw/ofami/M65846224.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a> 
				<br>
			</div>
		</div>
		<div class="row">
			<div id="afac1" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="col-sm-12">
						<a class="image-zoom" href="{{asset('images/afac1.jpg')}}" rel="prettyPhoto[gallery]">
							<img src="{{asset('images/afac1.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.afa')}}</h2>
				<p>{!!trans('nikimotion.afa1')!!}</p><br>
				<!--
				<p class="price">NTD. 1,399　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#afac1" class="btn btn-outline-secondary" role="button">Back</a>　
				<a href="http://www.pcstore.com.tw/ofami/M46669578.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<br>
			</div>
		</div>
		<div class="row">
			<div id="afac2" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<a class="image-zoom" href="{{asset('images/afac2.jpg')}}" rel="prettyPhoto[gallery]">
						<img src="{{asset('images/afac2.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
					</a>
				</div>
				<div class="col-lg-2"></div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.mn')}}</h2>
				<p>{!!trans('nikimotion.mn1')!!}</p><br>
				<!--
				<p class="price">NTD. 999　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#afac2" class="btn btn-outline-secondary" role="button">Back</a>　
				<a href="http://www.pcstore.com.tw/ofami/M46672783.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<br>
			</div>
		</div>
		<div class="row">
			<div id="afac3" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<a class="image-zoom" href="{{asset('images/afac3.jpg')}}" rel="prettyPhoto[gallery]">
						<img src="{{asset('images/afac3.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
					</a>
				</div>
				<div class="col-lg-2"></div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.rc')}}</h2>
				<p>{!!trans('nikimotion.rc1')!!}</p><br>
				<!--
				<p class="price">NTD. 1,299　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#afac3" class="btn btn-outline-secondary" role="button">Back</a>　
				<a href="http://www.pcstore.com.tw/ofami/M46673706.htm" class="btn btn-outline-primary" target="_blank" role="button"><i class="fa fa-shopping-cart"></i> 立即購買</a>
				<br>
			</div>
		</div>
		
	</div>
</section>
<!--//autofold accessories-->

<!--Autofoldlite Accessories-->
<!--
<section class="work py-5 my-lg-5">
	<div class="container">
		<h1 class="heading text-center text-uppercase mb-5"> {{trans('nikimotion.aflacc')}} </h1>
		<div class="row">
			<div id="aflac1" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="col-sm-12">
						<a class="image-zoom" href="{{asset('images/aflac1.jpg')}}" rel="prettyPhoto[gallery]">
							<img src="{{asset('images/aflac1.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.fb')}}</h2>
				<p>{!!trans('nikimotion.fb1')!!}</p><br>
				<a href="{{route('ofami.nikimotion',$locale)}}#aflac1" class="btn btn-outline-secondary" role="button">Back</a>
				<br>
			</div>
		</div>
	</div>
</section>-->
<!--autofoldlite accessories-->

<!--Blade Accessories-->
<section class="work py-5 my-lg-5">
	<div class="container">
	<h1 class="heading text-center text-uppercase mb-5"> {{trans('nikimotion.bladeacc')}} </h1>
		<div class="row">
			<div id="blac1" class="col-sm-6" align="center">
				<div class="col-lg-2"></div>
				<div class="col-lg-8">
					<div class="col-sm-12">
						<a class="image-zoom" href="{{asset('images/blac1.jpg')}}" rel="prettyPhoto[gallery]">
							<img src="{{asset('images/blac1.jpg')}}" class="img-responsive" alt="樂享學,ofami,嬰兒車,nikimotion,accessory">
						</a>
					</div>
				</div>
				<div class="col-lg-2"></div>
				<div align="right">
				</div>
				<p><br></p>
			</div>
			<div class="col-sm-6" align="left">
				<h2 class="my-2">{{trans('nikimotion.ba')}}</h2>
				<p>{!!trans('nikimotion.ba1')!!}</p><br>
				<!--
				<p class="price">NTD. 1,399　</p><br>-->
				<a href="{{route('ofami.nikimotion',$locale)}}#blac1" class="btn btn-outline-secondary" role="button">Back</a>
				<br>
			</div>
		</div>
	</div>
</section>
<!--//blade accessories-->


@endsection