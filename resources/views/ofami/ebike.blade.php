@extends("ofami.main")
@section('content')
@include("ofami.css.imghover")

<style>
    .row img{
        padding: 0.5em 1em;
    }
    .row h2{
        font-size:1.8em;
    }
</style>

<!-- inner page banner -->
<section class="inner_banner">
	<div class="dot1">
	</div>
</section>
<!-- inner page banner -->

<!-- ebike -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> E-Bike 電助腳踏車 </h1>
        <iframe class="wow jackInTheBox" data-wow-duration="2s" width="100%" height="500" src="https://www.youtube.com/embed/YXKk31SCX0Q?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
	</div>
</section>
<!-- ebike -->

<!-- ebike product -->
<section class="work py-5 my-lg-5">
	<div class="container">
        <h1 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> Product 產品 </h1>
		<div class="row">
			<div class="col-lg-3 col-sm-6 wow fadeInRight" data-wow-duration="2s" align="center">
                <a href="{{route('ofami.ebikeinfo',[$locale,$type="ebfat"])}}">
                    <div class="imgback">
                        <img src="{{asset('images/ebike5.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,胖胎車,ebike" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('ebike.bike11')!!}</div>
                        </div>
                    </div>
                </a>
				<p class=""></p>
				<p></p>
			</div>
			<div class="col-lg-3 col-sm-6 wow fadeInUp" data-wow-duration="2s" align="center">
                <a href="{{route('ofami.ebikeinfo',[$locale,$type="ebflat"])}}">
                    <div class="imgback">
                        <img src="{{asset('images/ebike3.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,平把,ebike" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('ebike.bike22')!!}</div>
                        </div>
                    </div>
                </a>
				<p class=""></p>
				<p></p>
			</div>
			<div class="col-lg-3 col-sm-6 wow fadeInDown" data-wow-duration="2s" align="center">
                <a href="{{route('ofami.ebikeinfo',[$locale,$type="ebcurve"])}}">
                    <div class="imgback">
                        <img src="{{asset('images/ebike4.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,彎把,ebike" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('ebike.bike33')!!}</div>
                        </div>
                    </div>
                </a>
				<p class=""></p>
				<p></p>
            </div>
			<div class="col-lg-3 col-sm-6 wow fadeInLeft" data-wow-duration="2s" align="center">
                <a href="{{route('ofami.ebikeinfo',[$locale,$type="eb26"])}}">
                    <div class="imgback">
                        <img src="{{asset('images/ikin ez i-bike/ez16.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,26吋,ebike" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('ebike.bike66')!!}</div>
                        </div>
                    </div>
                </a>
				<p class=""></p>
				<p></p>
            </div>
			{{-- <div class="col-lg-3 col-sm-6" align="center">
                <a href="{{route('ofami.ebikeinfo',[$locale,$type="eb20"])}}">
                    <div class="imgback">
                        <img src="{{asset('images/ebike1.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,電動輔助自行車,20吋,ebike" class="img-fluid">
                        <div class="overlay">
                            <div class="text">{!!trans('ebike.bike55')!!}</div>
                        </div>
                    </div>
                </a>
				<p class=""></p>
				<p></p>
            </div> --}}
		</div>
	</div>
</section>
<!-- ebike product -->

@endsection