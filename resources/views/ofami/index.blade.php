@extends("ofami.main")
@section('content')
<!--Slider-->
<div class="slider">
	<div class="callbacks_container">
		<ul class="rslides" id="slider3">
			<li>
				<div class="slider-img1">
					<div class="dot">
						<div class="container">
							<div class="slider_banner_info_w3ls text-center">
								<h1 class="text-uppercase wow zoomIn" data-wow-duration="2s">O-Fami</h1>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="slider-img2">
					<div class="dot">
						<div class="container">
							<div class="slider_banner_info_w3ls text-center">
								<h2 class="text-uppercase wow zoomIn" data-wow-duration="2s">Café</h2>
							</div>
						</div>
					</div>
				</div>
			</li>
			<li>
				<div class="slider-img3">
					<div class="dot">
						<div class="container">
							<div class="slider_banner_info_w3ls text-center">
								<h4 class="text-uppercase wow zoomIn" data-wow-duration="2s">Stroller</h4>
							</div>
						</div>
					</div>
				</div>
			</li>
		</ul>
	</div>
	<div class="clearfix"></div>
</div>
<!--//Slider-->
<!-- About us -->
<section class="about py-5">
	<div class="container">
		<h3 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp">{{trans('index.os')}}</h3>
		<p class="aboutpara wow fadeInUp" data-wow-duration="2s">　　樂享學是由一群感情緊密的夥伴所促成的店，我們都愛家，愛享受生活的休閒片刻；我們也愛小孩，愛孩子們喜悅時的笑顏；我們更愛戶外，愛和自己最親密的家人、朋友一同去野外探索這世間的奧妙。我們更喜歡相聚在一塊，一杯咖啡，簡單的手作食物，一起談天、說地，向不同領域的朋友學習充實自我。因此樂享學就這樣誕生了，我們希望藉由我們所代理回來的產品嬰兒車、拖車、電動輔助腳踏車，來促進父母與孩子之間的情感，增進朋友間的羈絆。就是這樣的單純、純粹，也希望光臨樂享學的每一個人，能夠跟我們一起享受歡聚的時光。</p>
		
		<div class="row about_grids mt-5">
			<div class="col-md-4 wow fadeInRight" data-wow-duration="2s">
				<img src="{{asset('images/pd1.jpg')}}" alt="樂享學,ofami,嬰兒車,nikimotion,veer" class="img-fluid" />
				<h3 class="mt-3 my-2 text-capitalize">{{trans('index.op')}}</h3>
				<a href="{{route("ofami.nikimotion",$locale)}}" class="text-capitalize"><span class="fas fa-long-arrow-alt-right"></span> Nikimotion </a>　
				<a href="{{route("ofami.veer",$locale)}}" class="text-capitalize"><span class="fas fa-long-arrow-alt-right"></span> Veer </a>
			</div>
			<div class="col-md-4 mt-md-0 mt-4 wow fadeInUp" data-wow-duration="2s">
				<img src="{{asset('images/pd2.jpg')}}" alt="樂享學,ofami,電動輔助腳踏車,ebike" class="img-fluid" />
				<h3 class="mt-3 my-2 text-capitalize">{{trans('index.ebike')}}</h3>
				<a href="{{route("ofami.ebike",$locale)}}" class="text-capitalize"><span class="fas fa-long-arrow-alt-right"></span> E-Bike </a>
			</div>
			<div class="col-md-4 mt-md-0 mt-4 wow fadeInLeft" data-wow-duration="2s">
				<img src="{{asset('images/pd3.jpg')}}" alt="樂享學,ofami,親子餐廳,咖啡輕食,cafe" class="img-fluid" />
				<h3 class="mt-3 my-2 text-capitalize">{{trans('index.cafe')}}</h3>
				<a href="{{route("ofami.cafe",$locale)}}" class="text-capitalize"><span class="fas fa-long-arrow-alt-right"></span> Café </a>
			</div>
		</div>
		
	</div>
</section>
<!-- //About us -->

<section class="about py-5">
	<div class="container">
		<h3 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp" data-wow-duration="2s"> {{trans('index.origin')}}</h3>
		<p class="aboutpara wow fadeInUp">　　樂享學O-Fami，是間顛覆大家既定印象的親子咖啡廳，結合了輕食早午餐、嬰幼兒推車以及電動輔助自行車，希望給予大家一個不一樣的現代休閒生活概念。O-Fami，聽起來有著孩童念著All Family牙牙學語般的稚氣，也有著我們樂享學這個空間希望能夠成為一個全家大小歡聚的場所。<br><br>　　O-Fami其中的O，是英文字母O，也有著outdoor的概念，由於樂享人們所賣的產品，不管是嬰兒推車、拖車還是電動輔助自行車，我們的產品希望能夠使每個家庭走出戶外，放下現代生活中的繁複枷鎖，和家人一同享受探索大自然的奧妙與趣味。而O這個圓，也有著輪子轉動的意象，所有樂享學的產品都有著輪子，轉動的輪子，意味著源源不絕、生生不息，代表著一家人圍著圈兒，手拉著手相聚在此刻，圓滿而喜悅。<br><br></p>
		<p style="float:right" class="wow fadeInUp" data-wow-duration="2s">－－「樂享學，讓我們一起享受人生的每一個日常。」</p><br><br>

		<div class="text-center wow jackInTheBox" data-wow-duration="2s">
			<iframe src="https://www.facebook.com/plugins/video.php?href=https%3A%2F%2Fwww.facebook.com%2Fo.fami.tw%2Fvideos%2F1943907622575315%2F&show_text=0&width=560" width="80%" height="500" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allowFullScreen="true"></iframe>
		</div><br><br>
	</div>
</section>

<section class="about py-5">
	<div class="container">
		<h3 id="size1" class="heading text-center text-uppercase mb-5 wow fadeInUp"> {{trans('index.wio')}}?</h3>
		<p class="aboutpara wow fadeInUp" data-wow-duration="2s">
			　　好多人都問我們，咖啡廳？嬰兒推車？拖車？電動輔助自行車？樂享學到底是在做什麼的呀？其實很簡單（笑），我們只賣『一家人』能夠一起使用的產品，一家人一同在咖啡廳享用香醇的咖啡與輕食；爸爸媽媽推著嬰兒推車在都市中穿梭度過假日的悠悠午後，全家大小拉著小拖車在公園中野餐玩耍。當孩子大了、父母老了，讓電動輔助自行車帶著你繼續完成年輕時未完成的攻頂夢，你會驚艷自己原來也可以這麼輕易的做到。這就是我們所想的。
		</p>
		<p class="wow fadeInUp" style="float:right" data-wow-duration="2s">－－「樂享學，樂享家，我們都會是自己生活的樂享人。」</p>
	</div>
</section>

<!-- How we Work -->
<div class="row wow zoomIn" data-wow-duration="2s">
	<div class="col-md-12">
		<!-- InstaWidget -->
		<a href="https://instawidget.net/v/user/o.fami.tw" id="link-ff1d179210e00908cf74c4fa6f31baa99fa3bca2845ec742ddebf7d148516138">@o.fami.tw</a>
		<script src="https://instawidget.net/js/instawidget.js?u=ff1d179210e00908cf74c4fa6f31baa99fa3bca2845ec742ddebf7d148516138&width=100%"></script>
	</div>
</div>

<iframe class="wow jackInTheBox" data-wow-duration="2s" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3671.7488843908427!2d120.25322841496808!3d23.032990684947865!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x346e71d9dec9de13%3A0x9452d692262750ab!2z5qiC5Lqr5a24!5e0!3m2!1szh-TW!2stw!4v1531213656168" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
@endsection