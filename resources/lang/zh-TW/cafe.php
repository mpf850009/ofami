<?php

return [

    'all'      => '總覽',
    'snack'    => '小食',
    'waffle'   => '鬆餅',
    'brunch'   => '早午餐',
    'sd'       => '特調',
    'tea'      => '茶品',
    'it'       => '限定冷泡茶',
    'cafe'     => '咖啡',
    'smoothie' => '冰沙',
    'soda'     => '氣泡飲品',
    'secret'     => '隱藏版&季節限定',
];
