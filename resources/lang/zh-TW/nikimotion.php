<?php

return [

    'fc'       => 'Fabric Color',
    'intro'    => 'Introduction',
    'af'       => '<strong>產品介紹</strong><br>
            <ul>
                <li>單手收折</li>
                <li>可調節式背靠及腳踏</li>
                <li>收折後可自行站立</li>
                <li>四輪避震設計</li>
                <li>快拆式前後輪</li>
                <li>可拆卸式幼兒手扶把</li>
                <li>可拆卸式清洗布料</li>
                <li>大空間置物籃</li>
                <li>開折式遮陽蓬共四折</li>
                <li>五點式安全帶</li>
                <li>防潑水及抗UV布料</li>
                <li>15種顏色可搭配選擇</li>
            </ul><br>
            <strong>規格</strong><br>
            <ul>
                <li>適用年齡 新生兒15KG</li>
                <li>推車重量 8.6 KG</li>
                <li>展車尺寸 91.5 57.5 105 cm</li>
                <li>收車尺寸 68 57.5 29 cm</li>
                <li>推車輪胎 四輪PU</li>
                <li>測試認證 EN1888-2012/CNS12940</li>
            </ul><br>
            
            <strong>配件</strong><br>
            <ul>
                <li>MAXI-COSI 汽座轉接座</li>
                <li>水杯架</li>
                <li>雨罩</li>
                <li>蚊帳</li>
                <li>手把置物包</li>
            </ul><br>',
    'afl'      => '<strong>產品介紹</strong><br>
            <ul>
                <li>單手收折</li>
                <li>可調節式背靠</li>
                <li>收折後可自行站立</li>
                <li>大空間置物籃</li>
                <li>五點式安全扣具</li>
                <li>可搭配配件睡箱</li>
            </ul><br>
            <strong>規格</strong><br>
            <ul>
                <li>重量 7.6 kg</li>
                <li>收折尺寸 68 x 55 x 30 cm</li>
                <li>車身寬度 > 60cm</li>
            </ul><br>
            <strong>配件</strong><br>
            <ul>
                <li>MAXI-COSI 汽座轉接座</li>
                <li>水杯架</li>
                <li>蚊帳</li>
                <li>雨罩</li>
            </ul><br>',
    'blade'    => '<strong>產品介紹</strong><br>
            <ul>
                <li>雙向使用座椅</li>
                <li>雙向座椅皆可直接收展車</li>
                <li>收車後可自行站立</li>
                <li>可調節式背靠</li>
                <li>可拆卸式手把</li>
                <li>天窗式遮陽蓬</li>
                <li>五點式安全扣具</li>
                <li>搭配配件睡箱</li>
            </ul><br>
            <strong>規格</strong><br>
            <ul>
                <li>重量	14.5 kg</li>
                <li>尺寸	90 x 54 x 29 cm</li>
            </ul><br>
            <!--
            <strong>配件</strong><br>
            <ul>
                <li>MAXI-COSI氣座轉接座</li>
                <li>水杯架</li>
                <li>蚊帳</li>
                <li>雨罩</li>
            </ul><br>-->',
    'ch'       => '杯架',
    'ch1'      => '杯架可快速安裝在嬰兒車把手側邊的卡榫上，放置爸爸媽媽的茶水、飲料。',
    'ch2'      => '手扶把',
    'ch3'      => '手扶把，為小朋友安全提供多一道防護。',
    'ch4'      => '手扶把置物包',
    'ch5'      => '手把置物包方便讓媽媽放置手機、錢包、鑰匙、飲料；防波水材質布料，讓媽媽們在下雨天時也能安心使用。',
    'afa'      => 'AUTOFOLD 汽座轉接座',
    'afa1'     => '安裝汽座轉接座可將Maxicosi的安全座椅放置在AUTOFOLD車上使用。<br>適用MAXI-COSI汽座型號 : Cabrio Fix, Pebble, Pebble Plus, Citi',
    'mn'       => '蚊帳',
    'mn1'      => '使用配件蚊帳可有效將蚊蟲隔絕在車身外，蚊帳材質為具有良好的通風效果的網狀布料。',
    'rc'       => '雨罩',
    'rc1'      => '雨罩可在下雨天和風大的天氣狀況下使用，雨罩採用PVC材質，容易清潔、收納。',
    'fb'       => 'AUTOFOLD LITE 手把',
    'fb1'      => 'Autofold LITE的手扶把。',
    'ba'       => 'BLADE 汽座轉接座',
    'ba1'      => '裝汽座轉接座可將Maxicosi的安全座椅放置在BLADE車上使用。<br>適用MAXI-COSI汽座型號 : Cabrio Fix, Pebble, Pebble Plus, Citi',
    'acc'      => 'Nikimotion 通用配件',
    'afacc'    => 'Autofold 專用配件',
    'aflacc'   => 'Autofold Lite 專用配件',
    'bladeacc' => 'Blade 專用配件',
];
