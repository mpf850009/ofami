<?php

return [
    'bike1'    => '胖胎登山越野車',
    'bike11'   => '胖胎<br>登山越野車',
    'bikerule' => '<ol>
                    <li>符合台灣法規</li>
                    <li>最高速度為25km/hr</li>
                    <li>400W中置馬達</li>
                    <li>需約5~6小時完全充飽電</li>
                    <li>最大輔助可行走約50公里</li>
                    <li>最低輔助可行走約140公里</li>
                </ol>
                <p style="font-size:18px;color:red;">* 騎乘距離會因地形、氣候、載重而有所不同，測試載重約70KG</p>',
    'bike1d'   => '<thead>
                        <tr>
                            <th>配件</th>
                            <th>規格</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>車架</td>
                            <td>鋁合金#6061</td>
                        </tr>
                        <tr>
                            <td>前叉</td>
                            <td>Renegade Air TRL 行程100mm 附線控</td>
                        </tr>
                        <tr>
                            <td>煞車組</td>
                            <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                        </tr>
                        <tr>
                            <td>變速</td>
                            <td>11速SHIMANO 105</td>
                        </tr>
                        <tr>
                            <td>齒盤</td>
                            <td>1/2*11/128吋*36T</td>
                        </tr>
                        <tr>
                            <td>鍊條</td>
                            <td>KMC X9e</td>
                        </tr>
                        <tr>
                            <td>坐墊柱</td>
                            <td>ETENi L400mm27.2 線控式可調座管</td>
                        </tr>
                        <tr>
                            <td>飛輪</td>
                            <td>11/34T</td>
                        </tr>
                        <tr>
                            <td>車手</td>
                            <td>鋁合金 31.8 700W ISO4210-M</td>
                        </tr>
                        <tr>
                            <td>座管</td>
                            <td>鋁合金 31.8 ISO4210-M</td>
                        </tr>
                        <tr>
                            <td>輪胎</td>
                            <td>26*4吋 / KENDA, L-1151, 26"*4", A/V, BK</td>
                        </tr>
                        <tr>
                            <td>輪圈</td>
                            <td>26*32H 單層圈 方形減重孔 BK</td>
                        </tr>
                        <tr>
                            <td>鋼絲</td>
                            <td>白鐵 BK / HSING TA, CP Nipple, BK</td>
                        </tr>
                        <tr>
                            <td>腳踏</td>
                            <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                        </tr>
                        <tr>
                            <td>停車架</td>
                            <td>側立 可調式 鋁 B</td>
                        </tr>
                        <tr>
                            <td>前燈</td>
                            <td>SSL127WH</td>
                        </tr>
                        <tr>
                            <td>長寬高</td>
                            <td>1850*650*1100 MM</td>
                        </tr>
                        <tr>
                            <td>重量</td>
                            <td>23.3KG</td>
                        </tr>
                    </tbody>',
    'bike2'    => '平把變速公路車',
    'bike22'   => '平把<br>變速公路車',
    'bike2d'   => '<thead>
                        <tr>
                            <th>配件</th>
                            <th>規格</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>車架</td>
                            <td>鋁合金#6061</td>
                        </tr>
                        <tr>
                            <td>前叉</td>
                            <td>Renegade Air TRL 行程100mm 附線控</td>
                        </tr>
                        <tr>
                            <td>煞車組</td>
                            <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                        </tr>
                        <tr>
                            <td>變速</td>
                            <td>11速SHIMANO 105</td>
                        </tr>
                        <tr>
                            <td>齒盤</td>
                            <td>1/2*11/128吋(inch)*34T</td>
                        </tr>
                        <tr>
                            <td>鍊條</td>
                            <td>KMC X11e</td>
                        </tr>
                        <tr>
                            <td>坐墊柱</td>
                            <td>ETENi L400mm27.2 線控式可調座管</td>
                        </tr>
                        <tr>
                            <td>飛輪</td>
                            <td>11/34T</td>
                        </tr>
                        <tr>
                            <td>車手</td>
                            <td>鋁合金 31.8 620W,640W,660W ISO4210-M</td>
                        </tr>
                        <tr>
                            <td>座管</td>
                            <td>鋁合金31.8 EX105 L41 ISO4210-M</td>
                        </tr>
                        <tr>
                            <td>輪胎</td>
                            <td>700C*35C</td>
                        </tr>
                        <tr>
                            <td>輪圈</td>
                            <td>700C*32H 雙層圈 方形減重孔 BK</td>
                        </tr>
                        <tr>
                            <td>鋼絲</td>
                            <td>白鐵 BK</td>
                        </tr>
                        <tr>
                            <td>腳踏</td>
                            <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                        </tr>
                        <tr>
                            <td>停車架</td>
                            <td>側立 可調式 鋁 B</td>
                        </tr>
                        <tr>
                            <td>前燈</td>
                            <td>SSL127WH</td>
                        </tr>
                        <tr>
                            <td>長寬高</td>
                            <td>1760*670*1050 MM</td>
                        </tr>
                        <tr>
                            <td>重量</td>
                            <td>21.5KG</td>
                        </tr>
                    </tbody>',
    'bike3'    => '彎把競速公路車',
    'bike33'   => '彎把<br>競速公路車',
    'bike3d'   => '<thead>
                        <tr>
                            <th>配件</th>
                            <th>規格</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>車架</td>
                            <td>鋁合金#6061</td>
                        </tr>
                        <tr>
                            <td>前叉</td>
                            <td>CARBON鋁豎</td>
                        </tr>
                        <tr>
                            <td>煞車組</td>
                            <td>BB5</td>
                        </tr>
                        <tr>
                            <td>變速</td>
                            <td>11速SHIMANO 105</td>
                        </tr>
                        <tr>
                            <td>齒盤</td>
                            <td>1/2*11/128吋*44/30T</td>
                        </tr>
                        <tr>
                            <td>鍊條</td>
                            <td>KMC X11e</td>
                        </tr>
                        <tr>
                            <td>坐墊柱</td>
                            <td>SPSL282 L350mm27.2 鋁</td>
                        </tr>
                        <tr>
                            <td>飛輪</td>
                            <td>11/32T</td>
                        </tr>
                        <tr>
                            <td>車手</td>
                            <td>鋁合金 31.8 420W , 440W ISO421</td>
                        </tr>
                        <tr>
                            <td>座管</td>
                            <td>鋁合金 31.8 EX807度 , 90 7度 L38 ISO4210M</td>
                        </tr>
                        <tr>
                            <td>輪胎</td>
                            <td>700C*32C</td>
                        </tr>
                        <tr>
                            <td>輪圈</td>
                            <td>700C*28H 雙層圈 BK</td>
                        </tr>
                        <tr>
                            <td>鋼絲</td>
                            <td>白鐵 BK / CP Nipple, BK</td>
                        </tr>
                        <tr>
                            <td>腳踏</td>
                            <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                        </tr>
                        <tr>
                            <td>停車架</td>
                            <td>側立 可調式 鋁 B</td>
                        </tr>
                        <tr>
                            <td>前燈</td>
                            <td>SSL127WH</td>
                        </tr>
                        <tr>
                            <td>長寬高</td>
                            <td>1750*480*960 MM</td>
                        </tr>
                        <tr>
                            <td>重量</td>
                            <td>19.5KG</td>
                        </tr>
                    </tbody>',
    'bike4'    => 'K&K胖胎車',
    'bike4d'   => '<thead>
                        <tr>
                            <th>配件</th>
                            <th>規格</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>車架</td>
                            <td>26" X 430 mm ALLOY FOR MPF MOTOR, H/T: 1-1/8"-1-1/2" </td>
                        </tr>
                        <tr>
                            <td>前叉</td>
                            <td>26" ALLOY BLADES/CR-MO,STEM, W/DISC MOUNT</td>
                        </tr>
                        <tr>
                            <td>手把</td>
                            <td>ALLOY MATERIAL 680W,RISE 20MM,OB 31.8MM</td>
                        </tr>
                        <tr>
                            <td>車把立管</td>
                            <td>ALLOY STEM, BK</td>
                        </tr>
                        <tr>
                            <td>輪框</td>
                            <td>26"*559*73,14G*32H A/V</td>
                        </tr>
                        <tr>
                            <td>輪胎</td>
                            <td>26"*4.0",SBK(72 TPI),FOLDABLE FB</td>
                        </tr>
                        <tr>
                            <td>鍊條</td>
                            <td>124L,FOR 10SPEED</td>
                        </tr>
                        <tr>
                            <td>曲柄</td>
                            <td>32</td>
                        </tr>
                        <tr>
                            <td>齒輪</td>
                            <td>RD X9 TYPE 2 LONG CAGE 10SPD BLK/RED</td>
                        </tr>
                        <tr>
                            <td>腳踏板</td>
                            <td>9/16",CR-MO AXLE,ALLOY BODY,W/BS REF,W/K-MARK</td>
                        </tr>
                        <tr>
                            <td>電池</td>
                            <td>PHYLION 36VX14AH</td>
                        </tr>
                        <tr>
                            <td>充電器</td>
                            <td>PHYLION CHARGER 2A</td>
                        </tr>
                        <tr>
                            <td>控制器</td>
                            <td>inside MOTOR</td>
                        </tr>
                        <tr>
                            <td>儀表</td>
                            <td>MATCH WITH MPF MOTOR</td>
                        </tr>
                        <tr>
                            <td>馬達</td>
                            <td>36VX450W,LIMITED SPEED 25KM/H</td>
                        </tr>
                        <tr>
                            <td>感應器</td>
                            <td>INSIDE MOTOR</td>
                        </tr>
                        <tr>
                            <td>剎車握把</td>
                            <td>BL SPEED DIA</td>
                        </tr>
                        <tr>
                            <td>前剎</td>
                            <td>DB BB7S 20I</td>
                        </tr>
                        <tr>
                            <td>後剎</td>
                            <td>DB BB7S 40I</td>
                        </tr>
                        <tr>
                            <td>座墊</td>
                            <td>1760NN1A-B BED BOW,W/O CLAMP,W/CR-MO RAIL</td>
                        </tr>
                        <tr>
                            <td>座墊豎管</td>
                            <td>ALLOY, BK</td>
                        </tr>
                    </tbody>',
    'bike5'    => '20吋小徑城市悠遊摺疊車',
    'bike55'   => '20吋E-BIKE',
    'bike5d'   => '<ol>
                    <li>36V無刷直流高效率馬達</li>
                    <li>250W中置馬達</li>
                    <li>行駛里程約30~80km</li>
                    <li>6段外變速</li>
                    <li>APP藍芽控制</li>
                    <li>APP緊急救援功能</li>
                </ol>',
    'bike6'    => 'ikin ez i-bike 電動輔助自行車',
    'bike66'   => 'ikin<br>ez i-bike',
    'bike6d'   => '<ol>
                    <li>36V無刷直流高效率馬達</li>
                    <li>400W中置馬達</li>
                    <li>行駛里程約30~80km</li>
                    <li>6段外變速</li>
                    <li>APP藍芽控制</li>
                    <li>APP緊急救援功能</li>
                </ol>',
];
