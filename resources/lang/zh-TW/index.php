<?php

return [

    'home'       => '首頁',
    'product'    => '嬰幼兒產品',
    'ebike'      => '電助腳踏車',
    'cafe'       => '咖啡輕食',
    'bikerental' => '租車旅遊',
    'sc'         => '購物網',
    'contact'    => '聯絡我們',
    'os'         => 'our story 關於我們',
    'origin'     => 'origin 起源',
    'wio'        => 'what is o-fami 樂享學是什麼?',
    'op'         => '嬰幼兒產品',
];
