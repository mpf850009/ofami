<?php

return [

    'fc'       => 'Fabric Color',
    'intro'    => 'Introduction',
    'af'       => '<strong>Description</strong><br>
				<ul>
					<li>Easy folding system with “central folding belt”</li>
					<li>Continuously adjustable backrest (50cm long)</li>
					<li>Stands when folded</li>
					<li>Removable and washable fabric parts</li>
					<li>Front bar detachable and can be opened from both sides</li>
					<li>Large shopping basket below the backrest and underneath the seat</li>
					<li>Viewing window on the roof-back</li>
					<li>5-point belt system</li>
					<li>Pram optional available</li>
				</ul>
				<br>
				<strong>Product Spec</strong><br>
				<ul>
					<li>Weight 8.6 kg</li>
					<li>Dimensions 68 x 59 x 30 cm</li>
					<li>Width > 60cm</li>
				</ul><br>
				<!--
				<strong>Accessory</strong><br>
				<ul>
					<li>MAXI-COSI Car Seat Adapter</li>
					<li>Cup Holder</li>
					<li>Bug Net</li>
					<li>Rain Cover</li>
				</ul><br>-->',
    'afl'      => '<strong>Description</strong><br>
				<ul>
					<li>Easy folding system with “central folding belt”</li>
					<li>Continuously adjustable backrest (50cm long)</li>
					<li>Stands when folded</li>
					<li>Large shopping basket below the backrest and underneath the seat</li>
					<li>5-point belt system</li>
					<li>Pram optional available</li>
				</ul><br>
				<strong>Product Spec</strong><br>
				<ul>
					<li>Weight 7.6 kg</li>
					<li>Dimensions 68 x 55 x 30 cm</li>
					<li>Width > 60cm</li>
				</ul><br>
				<!--
				<strong>Accessory</strong><br>
				<ul>
					<li>MAXI-COSI Car Seat Adapter</li>
					<li>Cup Holder</li>
					<li>Bug Net</li>
					<li>Rain Cover</li>
				</ul><br>-->',
    'blade'    => '<strong>Description</strong><br>
				<ul>
					<li>Reversible seat</li>
					<li>Stroller can be folded with seat in both directions</li>
					<li>Stands when folded</li>
					<li>One-handed back rest adjustment</li>
					<li>Front bar detachable and can be opened from both sides</li>
					<li>Viewing window on the roof-back</li>
					<li>5-point belt system</li>
					<li>Pram optional available</li>
				</ul><br>
				<strong>Product spec</strong><br>
				<ul>
					<li>Weight	14.5 kg</li>
					<li>Dimention	90 x 54 x 29 cm</li>
				</ul><br>
				<!--
				<strong>Accessory</strong><br>
				<ul>
					<li>MAXI-COSI Car Seat Adapter</li>
					<li>Cup Holder</li>
					<li>Bug Net</li>
					<li>Rain Cover</li>
				</ul><br>-->',
    'ch'       => 'CUP HOLDER',
    'ch1'      => '<strong>The Cup Holder from nikimotion is ideal for your and your darlings drinks!</strong><br><br>
					To be attached to the side of the pushchair by means of the fastening hook on the handle',
    'afa'      => 'AUTOFOLD ADAPTER',
    'afa1'     => '<strong>The Adapter for your AUTOFOLD stroller for setting the Baby Pram or Maxi Cosi.</strong><br><br>* The adapter allows the Maxi Cosi baby carrier (Cabrio Fix, Pebble, Pebble Plus, Citi)',
    'mn'       => 'MOSQUITO NET',
    'mn1'      => '<strong>The mosquito net for the nikimotion AUTOFOLD is a reliable protection against insects.</strong><br><br><ul>
					<li>fine mesh material for optimum ventilation</li>
					<li>reclosable storage bag</li></ul>',
    'rc'       => 'RAINCOVER',
    'rc1'      => '<strong>The AUTOFOLD raincover protects your darling against rain, wind and snow. It fits perfectly on the nikimotion AUTOFOLD and AUTOFOLD LITE stroller.</strong><br><br><ul>
					<li>PVC-free</li>
					<li>great windowed with vendilation</li>
					<li>easy to clean</li>
					<li>protection of rain, snow and wind</li>
					<li>age recommendation: from birth</li>
					<li>color: Transparent</li>
					<li>also suitable for AUTOFOLD LITE</li></ul>',
    'fb'       => 'AUTOFOLD LITE FRONTBAR',
    'fb1'      => '<strong>Front bar for Autofold Lite black</strong>',
    'ba'       => 'BLADE ADAPTER',
    'ba1'      => '<strong>The Adapter for your nikimotion BLADE stroller for setting the Baby Pram or Maxi Cosi</strong><br><br>* The adapter allows the Maxi Cosi baby carrier (Cabrio Fix, Pebble, Pebble Plus, Citi)',
    'acc'      => 'Nikimotion Accessory',
    'afacc'    => 'Autofold Accessory',
    'aflacc'   => 'Autofold Lite Accessory',
    'bladeacc' => 'Blade Accessory',
];
