<?php

return [
    'bike1'    => 'Fat E-Bike',
    'bike11'   => 'Fat E-Bike',
    'bikerule' => '<ol class="hideme">
                    <li>Meet Taiwan Regulation</li>
                    <li>Maximum speed 25km/hr</li>
                    <li>400W mid motor</li>
                    <li>Recharging Time: 5~6 hours</li>
                    <li>Riding range at 10th support level: 50km</li>
                    <li>Riding range at 1st support level: 140km</li>
                </ol>
                <p class="hideme" style="font-size:18px;color:red;">* The riding range will be different due to terrain, weather, and loading.</p>',
    'bike1d'   => '<thead class="hideme">
                        <tr>
                            <th>Accessories</th>
                            <th>Spec</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="hideme">
                            <td>Frame</td>
                            <td>ASAMA, 26"432mm, Alloy</td>
                        </tr>
                        <tr class="hideme">
                            <td>Front Fork</td>
                            <td>Alloy, Travel: 100mm</td>
                        </tr>
                        <tr class="hideme">
                            <td>Brake</td>
                            <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                        </tr>
                        <tr class="hideme">
                            <td>Groupset</td>
                            <td>11速SHIMANO 105</td>
                        </tr>
                        <tr class="hideme">
                            <td>Crankset</td>
                            <td>1/2*11/128&quot;*36T</td>
                        </tr>
                        <tr class="hideme">
                            <td>Chain</td>
                            <td>KMC X9e</td>
                        </tr>
                        <tr class="hideme">
                            <td>Saddle Post</td>
                            <td>ETENi L400mm27.2 線控式可調座管</td>
                        </tr>
                        <tr class="hideme">
                            <td>Cassette</td>
                            <td>11/34T</td>
                        </tr>
                        <tr class="hideme">
                            <td>Handlebar</td>
                            <td>Aluminum Alloy 31.8 700W ISO4210-M</td>
                        </tr>
                        <tr class="hideme">
                            <td>Seatpost</td>
                            <td>Aluminum Alloy 31.8 ISO4210-M</td>
                        </tr>
                        <tr class="hideme">
                            <td>Tires</td>
                            <td>26*4&quot; / KENDA, L-1151, 26&quot;*4&quot;, A/V, BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Wheelset</td>
                            <td>26*32H BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Spokes</td>
                            <td>HSING TA, CP Nipple, BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Pedals</td>
                            <td>NWL-303B, ISO4210</td>
                        </tr>
                        <tr class="hideme">
                            <td>Stands</td>
                            <td>側立 可調式 鋁 B</td>
                        </tr>
                        <tr class="hideme">
                            <td>Head Lamp</td>
                            <td>SSL127WH</td>
                        </tr>
                        <tr class="hideme">
                            <td>Dimension</td>
                            <td>1850*650*1100 MM</td>
                        </tr>
                        <tr class="hideme">
                            <td>Weight</td>
                            <td>23.3KG</td>
                        </tr>
                    </tbody>',
    'bike2'    => 'Flat E-Bike',
    'bike22'   => 'Flat E-Bike',
    'bike2d'   => '<thead class="hideme">
                        <tr>
                            <th>Accessories</th>
                            <th>Spec</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="hideme">
                            <td>Frame</td>
                            <td>ASAMA, 27"*500mm, Alloy, ISO4210-C</td>
                        </tr>
                        <tr class="hideme">
                            <td>Front Fork</td>
                            <td>700C, Alloy</td>
                        </tr>
                        <tr class="hideme">
                            <td>Brake</td>
                            <td>SHIMANO 拉柄BLM365剎車器BRM36</td>
                        </tr>
                        <tr class="hideme">
                            <td>Groupset</td>
                            <td>11速SHIMANO 105</td>
                        </tr>
                        <tr class="hideme">
                            <td>Crankset</td>
                            <td>1/2*11/128&quot;*34T</td>
                        </tr>
                        <tr class="hideme">
                            <td>Chain</td>
                            <td>KMC X11e</td>
                        </tr>
                        <tr class="hideme">
                            <td>Saddle Post</td>
                            <td>ETENi L400mm27.2</td>
                        </tr>
                        <tr class="hideme">
                            <td>Cassette</td>
                            <td>11/34T</td>
                        </tr>
                        <tr class="hideme">
                            <td>Handlebar</td>
                            <td>31.8 620W,640W,660W ISO4210-M</td>
                        </tr>
                        <tr class="hideme">
                            <td>Seatpost</td>
                            <td>31.8 EX105 L41 ISO4210-M</td>
                        </tr>
                        <tr class="hideme">
                            <td>Tires</td>
                            <td>700C*35C</td>
                        </tr>
                        <tr class="hideme">
                            <td>Wheelset</td>
                            <td>700C*32H BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Spokes</td>
                            <td>CP Nipple, Steel</td>
                        </tr>
                        <tr class="hideme">
                            <td>Pedals</td>
                            <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                        </tr>
                        <tr class="hideme">
                            <td>Stands</td>
                            <td>側立 可調式 鋁 B</td>
                        </tr>
                        <tr class="hideme">
                            <td>Head Lamp</td>
                            <td>SSL127WH</td>
                        </tr>
                        <tr class="hideme">
                            <td>Dimension</td>
                            <td>1760*670*1050 MM</td>
                        </tr>
                        <tr class="hideme">
                            <td>Weight</td>
                            <td>21.5KG</td>
                        </tr>
                    </tbody>',
    'bike3'    => 'Curve E-Bike',
    'bike33'   => 'Curve E-Bike',
    'bike3d'   => '<thead class="hideme">
                        <tr>
                            <th>Accessories</th>
                            <th>Spec</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="hideme">
                            <td>Frame</td>
                            <td>700C*520mm, Alloy</td>
                        </tr>
                        <tr class="hideme">
                            <td>Front Fork</td>
                            <td>CARBON</td>
                        </tr>
                        <tr class="hideme">
                            <td>Brake</td>
                            <td>BB5</td>
                        </tr>
                        <tr class="hideme">
                            <td>Groupset</td>
                            <td>11速SHIMANO 105</td>
                        </tr>
                        <tr class="hideme">
                            <td>Crankset</td>
                            <td>1/2*11/128&quot;*44/30T</td>
                        </tr>
                        <tr class="hideme">
                            <td>Chain</td>
                            <td>KMC X11e</td>
                        </tr>
                        <tr class="hideme">
                            <td>Saddle Post</td>
                            <td>SPSL282 L350mm27.2 鋁</td>
                        </tr>
                        <tr class="hideme">
                            <td>Cassette</td>
                            <td>11/32T</td>
                        </tr>
                        <tr class="hideme">
                            <td>Handlebar</td>
                            <td>鋁合金 31.8 420W , 440W ISO421</td>
                        </tr>
                        <tr class="hideme">
                            <td>Seatpost</td>
                            <td>鋁合金 31.8 EX807度 , 90 7度 L38 ISO4210M</td>
                        </tr>
                        <tr class="hideme">
                            <td>Tires</td>
                            <td>700C*32C</td>
                        </tr>
                        <tr class="hideme">
                            <td>Wheelset</td>
                            <td>700C*28H 雙層圈 BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Spokes</td>
                            <td>CP Nipple, BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Pedals</td>
                            <td>∮916 CRMO軸心 鋁 BK ISO4210</td>
                        </tr>
                        <tr class="hideme">
                            <td>Stands</td>
                            <td>側立 可調式 鋁 B</td>
                        </tr>
                        <tr class="hideme">
                            <td>Head Lamp</td>
                            <td>SSL127WH</td>
                        </tr>
                        <tr class="hideme">
                            <td>Dimension</td>
                            <td>1750*480*960 MM</td>
                        </tr>
                        <tr class="hideme">
                            <td>Weight</td>
                            <td>19.5KG</td>
                        </tr>
                    </tbody>',
    'bike4'    => 'K&K E-Bike',
    'bike4d'   => '<thead class="hideme">
                        <tr>
                            <th>Accessories</th>
                            <th>Spec</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr class="hideme">
                            <td>Frame</td>
                            <td>26" X 430 mm ALLOY FOR MPF MOTOR, H/T: 1-1/8"-1-1/2" </td>
                        </tr>
                        <tr class="hideme">
                            <td>Front Fork</td>
                            <td>26" ALLOY BLADES/CR-MO,STEM, W/DISC MOUNT</td>
                        </tr>
                        <tr class="hideme">
                            <td>Handle</td>
                            <td>ALLOY MATERIAL 680W,RISE 20MM,OB 31.8MM</td>
                        </tr>
                        <tr class="hideme">
                            <td>Stem</td>
                            <td>ALLOY STEM, BK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Rim</td>
                            <td>26"*559*73,14G*32H A/V</td>
                        </tr>
                        <tr class="hideme">
                            <td>Tires</td>
                            <td>26"*4.0",SBK(72 TPI),FOLDABLE FB</td>
                        </tr>
                        <tr class="hideme">
                            <td>Chain</td>
                            <td>124L,FOR 10SPEED</td>
                        </tr>
                        <tr class="hideme">
                            <td>Crankset</td>
                            <td>32</td>
                        </tr>
                        <tr class="hideme">
                            <td>Gear</td>
                            <td>RD X9 TYPE 2 LONG CAGE 10SPD BLK/RED</td>
                        </tr>
                        <tr class="hideme">
                            <td>Pedal</td>
                            <td>9/16",CR-MO AXLE,ALLOY BODY,W/BS REF,W/K-MARK</td>
                        </tr>
                        <tr class="hideme">
                            <td>Battery</td>
                            <td>PHYLION 36VX14AH</td>
                        </tr>
                        <tr class="hideme">
                            <td>Charger</td>
                            <td>PHYLION CHARGER 2A</td>
                        </tr>
                        <tr class="hideme">
                            <td>Controller</td>
                            <td>inside MOTOR</td>
                        </tr>
                        <tr class="hideme">
                            <td>Display</td>
                            <td>MATCH WITH MPF MOTOR</td>
                        </tr>
                        <tr class="hideme">
                            <td>Motor</td>
                            <td>36VX450W,LIMITED SPEED 25KM/H</td>
                        </tr>
                        <tr class="hideme">
                            <td>Sensor</td>
                            <td>INSIDE MOTOR</td>
                        </tr>
                        <tr class="hideme">
                            <td>Brake Lever</td>
                            <td>BL SPEED DIA</td>
                        </tr>
                        <tr class="hideme">
                            <td>Front Brake</td>
                            <td>DB BB7S 20I</td>
                        </tr>
                        <tr class="hideme">
                            <td>Rear Brake</td>
                            <td>DB BB7S 40I</td>
                        </tr>
                        <tr class="hideme">
                            <td>Saddle</td>
                            <td>1760NN1A-B BED BOW,W/O CLAMP,W/CR-MO RAIL</td>
                        </tr>
                        <tr class="hideme">
                            <td>Saddle Post</td>
                            <td>ALLOY, BK</td>
                        </tr>
                    </tbody>',
    'bike5'    => '20"E-Bike',
    'bike55'   => '20"E-Bike',
    'bike5d'   => '<ol class="hideme">
                    <li>36V brushless DC motor</li>
                    <li>250W mid motor</li>
                    <li>Riding range 30~80km</li>
                    <li>Shimano Derailleur 6 speed</li>
                    <li>APP Bluetooth control</li>
                    <li>APP SOS</li>
                </ol>',
    'bike6'    => 'ikin ez i-bike',
    'bike66'   => 'ikin<br>ez i-bike',
    'bike6d'   => '<ol class="hideme">
                    <li>36V brushless DC motor</li>
                    <li>400W mid motor</li>
                    <li>Riding range 30~80km</li>
                    <li>Shimano Derailleur 6 speed</li>
                    <li>APP Bluetooth control</li>
                    <li>APP SOS</li>
                </ol>',
];
