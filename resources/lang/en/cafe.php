<?php

return [

    'all'      => 'All',
    'snack'    => 'Snack',
    'waffle'   => 'Waffle',
    'brunch'   => 'Brunch',
    'sd'       => 'Special Drink',
    'tea'      => 'Tea',
    'it'       => 'Iced Tea',
    'cafe'     => 'Caffé',
    'smoothie' => 'Smoothie',
    'soda'     => 'Soda',
    'secret'   => 'Secret Menu',
];
