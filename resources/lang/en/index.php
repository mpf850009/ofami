<?php

return [

    'home'       => 'Home',
    'product'    => 'Stroller',
    'ebike'      => 'E-Bike',
    'cafe'       => 'Café',
    'bikerental' => 'Bikerental',
    'sc'         => 'Shopping',
    'contact'    => 'Contact',
    'os'         => 'our story',
    'origin'     => 'origin',
    'wio'        => 'what is o-fami',
    'op'         => 'Our Product',
];
